//
//  ReceiptDetailsViewController.swift
//  ReceiptScanner
//
//  Created by Andrei Budisan on 13/05/2020.
//  Copyright © 2020 Andrei Budisan. All rights reserved.
//

import UIKit
import MapKit
import Model

class ReceiptDetailsViewController: UIViewController {
  
  @IBOutlet private weak var mapView: MKMapView!
  @IBOutlet private weak var dateLabel: UILabel!
  @IBOutlet private weak var valueLabel: UILabel!
  
  var currentReceipt: Receipt?
  var onEditAction: (() -> Void)?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    setupNavigationBar()
    setupValuesInLabels()
  }
}

// MARK: - Button Action

extension ReceiptDetailsViewController {
  
  @objc
  private func onEditButtonPressed() {
    
    let editViewController = AddEditReceiptViewController.instantiate()
    editViewController.currentReceipt = currentReceipt
    editViewController.onEditAction = { [weak self] newReceipt in
      self?.currentReceipt = newReceipt
      self?.setupValuesInLabels()
      self?.onEditAction?()
    }
    navigationController?.pushViewController(editViewController, animated: true)
  }
}

// MARK: - SetupUI

extension ReceiptDetailsViewController {
  
  private func setupNavigationBar() {
    
    navigationItem.title = "Receipt Details"
    navigationController?.navigationBar.barTintColor = UIColor(named: "Main")
    navigationController?.navigationBar.tintColor = .white
    navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.white]
    
    let editButton = UIBarButtonItem(barButtonSystemItem: .edit,
                                     target: self,
                                     action: #selector(onEditButtonPressed))
    navigationItem.rightBarButtonItem = editButton
  }
  
  private func setupValuesInLabels() {
    
    if let currentReceipt = currentReceipt {
      
      centerMap(on: currentReceipt.location)
      
      let dateFormatter = DateFormatter()
      dateFormatter.dateFormat = "hh:mm dd-MM-yyyy"
      dateLabel.text = dateFormatter.string(from: currentReceipt.paymentTerm)
      
      valueLabel.text = String(format: "%.2f", currentReceipt.value)
    }
  }
}

// MARK: - Behavior

extension ReceiptDetailsViewController {
  
  private func centerMap(on location: CLLocationCoordinate2D) {
    
    guard CLLocationCoordinate2DIsValid(location) else {
      log.warning("Location coordinates \(location) are invalid.")
      return
    }
    
    let region = MKCoordinateRegion(center: location, latitudinalMeters: 1000, longitudinalMeters: 1000)
    mapView.setRegion(region, animated: true)
    
    let pinLocation = MKPointAnnotation()
    pinLocation.coordinate = location
    
    mapView.addAnnotation(pinLocation)
  }
}
