//
//  OpenCVWrapper.h
//  ReceiptScanner
//
//  Created by Andrei Budisan on 10/05/2020.
//  Copyright © 2020 Andrei Budisan. All rights reserved.
//

#ifndef OpenCVWrapper_h
#define OpenCVWrapper_h

#endif /* OpenCVWrapper_h */

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface OpenCVWrapper: NSObject
+ (UIImage *)resize:(UIImage*)image;
+ (UIImage *)prepareImageForOCR:(UIImage*)image;
@end

