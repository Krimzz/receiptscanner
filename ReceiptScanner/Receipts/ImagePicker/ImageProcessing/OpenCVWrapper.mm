//
//  OpenCVWrapper.mm
//  ReceiptScanner
//
//  Created by Andrei Budisan on 10/05/2020.
//  Copyright © 2020 Andrei Budisan. All rights reserved.
//

#import <opencv2/opencv.hpp>
#import <opencv2/imgcodecs/ios.h>
#import <Foundation/Foundation.h>
#import "OpenCVWrapper.h"

@implementation OpenCVWrapper

+ (UIImage *)resize:(UIImage*)image
{
  cv::Mat_<uchar> originalMatrix;
  UIImageToMat(image, originalMatrix);
  
  cv::Mat_<uchar> newMatrix(originalMatrix.rows, originalMatrix.cols);
  
  cv::resize(originalMatrix, newMatrix, cv::Size(), 0.50, 0.50);
  
  return MatToUIImage(newMatrix);
}

+ (UIImage *)prepareImageForOCR:(UIImage*)image
{
  cv::Mat_<cv::Vec3b> originalMatrix;
  UIImageToMat(image, originalMatrix);
  
  cv::Mat_<uchar> greyscaleMatrix(originalMatrix.rows, originalMatrix.cols, CV_8UC1);
  cv::cvtColor(originalMatrix, greyscaleMatrix, cv::COLOR_BGR2GRAY);
  
  cv::Mat_<uchar> canney;
  CanneyThreshold th;
  th = [self findCanneyThreshold: greyscaleMatrix];
  cv::Canny(greyscaleMatrix, canney, th.thresholdLow, th.thresholdHigh, 3);
  
  cv::Mat_<uchar> dilationIMG = [self dilation: canney];
  
  std::vector<std::vector<cv::Point>> contours;
  cv::findContours(dilationIMG, contours, cv::RETR_TREE, cv::CHAIN_APPROX_SIMPLE);
  
  NSUInteger max = 0;
  int maxPos = 0;
  for (int i = 0; i < contours.size(); i++) {
    if (max < contours[i].size()) {
      max = contours[i].size();
      maxPos = i;
    }
  }
  
  int sumLeft = 0;
  int sumLeftCount = 0;
  
  int sumUp = 0;
  int sumUpCount = 0;
  
  int sumDown = 0;
  int sumDownCount = 0;
  
  int sumRight = 0;
  int sumRightCount = 0;
  
  for (int i = 0; i < contours[maxPos].size(); i++) {
    if (contours[maxPos][i].x < dilationIMG.cols * 4 / 10) {
      sumLeft += contours[maxPos][i].x;
      sumLeftCount++;
    }
    else {
      
      if (contours[maxPos][i].x > dilationIMG.cols * 6 / 10 && contours[maxPos][i].x < dilationIMG.cols) {
        sumRight += contours[maxPos][i].x;
        sumRightCount++;
      }
    }
    
    if (contours[maxPos][i].y < dilationIMG.rows / 4) {
      sumUp += contours[maxPos][i].y;
      sumUpCount++;
    }
    else {
      if (contours[maxPos][i].y > dilationIMG.rows * 3 / 4 && contours[maxPos][i].y < dilationIMG.rows) {
        sumDown += contours[maxPos][i].y;
        sumDownCount++;
      }
    }
  }
  
  int newLeftX = sumLeft / sumLeftCount;
  int newRightX = sumRight / sumRightCount;
  
  int newUpY = sumUp / sumUpCount;
  int newDownY = sumDown / sumDownCount;
  
  cv::Rect boundRec(newLeftX, newUpY, newRightX - newLeftX, newDownY - newUpY);
  
  cv::Mat cropped = greyscaleMatrix(boundRec);
  
  int threshold = [self getLabelingThreshold: cropped];
  cv::Mat_<uchar> binarizedImage = [self binarize: cropped threshold: threshold];
  
  return MatToUIImage(binarizedImage);
}

typedef struct
{
  int thresholdLow;
  int thresholdHigh;
} CanneyThreshold;

#define MAX_VAL 1000000

+ (CanneyThreshold)findCanneyThreshold:(cv::Mat)image
{
  int* histo = (int*)malloc(256 * sizeof(int));

  for (int i = 0; i < 256; i++) {
    histo[i] = 0;
  }

  for (int i = 0; i < image.rows; i++) {
    for (int j = 0; j < image.cols; j++) {
      histo[image.at<uchar>(i, j)]++;
    }
  }

  int min = MAX_VAL;
  int minPos = 0;
  for (int i = 0; i < 128; i++) {
    if (min > histo[i]) {
      min = histo[i];
      minPos = i;
    }
  }

  int minR = MAX_VAL;
  int minPosR = 0;
  for (int i = 128; i < 256; i++) {
    if (minR > histo[i]) {
      minR = histo[i];
      minPosR = i;
    }
  }

  CanneyThreshold th;
  th.thresholdLow = minPos;
  th.thresholdHigh = minPosR;

  return th;
}

+ (int*)getHist:(cv::Mat)image {
  int* hist = (int*)malloc(256 * sizeof(int));

  for (int i = 0; i < 256; i++) {
    hist[i] = 0;
  }

  for (int i = 0; i < image.rows; i++) {
    for (int j = 0; j < image.cols; j++) {
      hist[image.at<uchar>(i, j)]++;
    }
  }
  return hist;
}


+ (int)getLabelingThreshold:(cv::Mat)image {

  int* hist = [self getHist: image];
  int i_max = 0;
  int i_min = 256;
  int threshold;
  int old_threshold;
  float error = 0.1, mean_low, M_low, mean_high, M_high;

  for (int i = 0; i < 256; i++) {

    if (i_min > i&& hist[i] != 0) {
      i_min = i;
    }

    if (i_max < i && hist[i] != 0) {
      i_max = i;
    }
  }

  threshold = (i_max + i_min) / 2;

  do {
    M_low = 0;
    M_high = 0;
    mean_low = 0;
    mean_high = 0;
    for (int i = i_min; i <= threshold; i++) {
      M_low += hist[i];
      mean_low += i * hist[i];
    }

    for (int i = threshold + 1; i < i_max; i++) {
      M_high += hist[i];
      mean_high += i * hist[i];
    }

    mean_low /= M_low;
    mean_high /= M_high;

    old_threshold = threshold;
    threshold = (mean_low + mean_high) / 2;

  } while (abs(threshold - old_threshold) < error);

  return threshold;
}

+ (cv::Mat_<uchar>)binarize:(cv::Mat_<uchar>)image threshold:(int) threshold
{
  cv::Mat_<uchar> blackwhite(image.rows, image.cols, CV_8UC1);

  for (int i = 0; i < image.rows; i++) {
    for (int j = 0; j < image.cols; j++) {
      if (image.at<uchar>(i, j) >= threshold) {
        blackwhite.at<uchar>(i, j) = 255;
      }
      else {
        blackwhite.at<uchar>(i, j) = 0;
      }
    }
  }
  return blackwhite;
}


+ (bool)isInside:(cv::Mat)img a:(int)a b:(int) b {
  
  if (a >= 0 && a < img.rows && b >= 0 && b < img.cols) {
    return true;
  }
  return false;
  
}

int di_4[4] = { -1,0,1,0 };
int dj_4[4] = { 0,-1,0,1 };

+ (cv::Mat_<uchar>)dilation:(cv::Mat_<uchar>) img {
  cv::Mat_<uchar> result = img.clone();

  for (int i = 0; i < img.rows; i++) {
    for (int j = 0; j < img.cols; j++) {
      if (img(i, j) == 255) {
        for (int k = 0; k < 4; k++) {
          if ([self isInside: img a: i + di_4[k] b: j + dj_4[k]]) {
            result(i + di_4[k], j + dj_4[k]) = img(i, j);
          }
        }
      }
    }
  }

  return result;
}

@end
