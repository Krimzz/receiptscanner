//
//  RSImagePickerViewController.swift
//  ReceiptScanner
//
//  Created by Andrei Budisan on 10/05/2020.
//  Copyright © 2020 Andrei Budisan. All rights reserved.
//

import UIKit

protocol ImagePickerDelegate: class {
  func pickedImage(location: URL)
}

class RSImagePickerViewController: UIImagePickerController, UINavigationControllerDelegate {
  
  weak var pickerDelegate: ImagePickerDelegate?
  
  override func viewDidLoad() {
    super.viewDidLoad()

    delegate = self
    mediaTypes = ["public.image"]
  }
}

// MARK: - UIImagePickerControllerDelegate

extension RSImagePickerViewController: UIImagePickerControllerDelegate {

  func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
    dismiss(animated: true)
  }

  func imagePickerController(_ picker: UIImagePickerController,
                             didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {

    dismiss(animated: true) {
      
      switch self.sourceType {
        
      case .camera:
        guard let imageURL = (info[.originalImage] as? UIImage)?.temporaryImageURL else { return }
        self.pickerDelegate?.pickedImage(location: imageURL)
        
      case .photoLibrary:
        guard let imageURL = info[.imageURL] as? URL else { return }
        self.pickerDelegate?.pickedImage(location: imageURL)
        
      default:
        return
      }
    }
  }
}

// MARK: - UIImage path

extension UIImage {

  fileprivate var temporaryImageURL: URL! {

    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd-HH-mm-ss"
    let imageName = dateFormatter.string(from: Date())

    let fileURL = URL(fileURLWithPath: NSTemporaryDirectory())
      .appendingPathComponent(imageName + ".jpg")

    guard let imageData = self.jpegData(compressionQuality: 1) else {
      return nil
    }

    do {
      try imageData.write(to: fileURL)
      return fileURL
    } catch {
      log.error("Failed saving captured image with error: \(error)")
      return nil
    }
  }
}
