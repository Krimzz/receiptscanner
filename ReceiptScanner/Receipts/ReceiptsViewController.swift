//
//  ReceiptsViewController.swift
//  ReceiptScanner
//
//  Created by Andrei Budisan on 26/04/2020.
//  Copyright © 2020 Andrei Budisan. All rights reserved.
//

import UIKit
import ViewModel
import RxRelay
import RxSwift
import NotificationBannerSwift

class ReceiptsViewController: UIViewController {
  
  @IBOutlet private weak var tableView: UITableView!
  @IBOutlet private weak var noReceiptsLabel: UIView!
  
  private let viewModel = ViewModelFactory.makeReceiptViewModel()
  private let refreshControl = UIRefreshControl()
  private var disposeBag = DisposeBag()
  private let loadingOverlay = LoadingOverlay()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    setupTableView()
    bindTableView()
  }

  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)

    tableView.refreshControl?.endRefreshing()
  }

}

// MARK: - Button Action

extension ReceiptsViewController {
  
  @IBAction private func onAddReceiptPressed(_ sender: UIBarButtonItem) {
    
    let addReceiptViewController = AddEditReceiptViewController.instantiate()
    addReceiptViewController.onAddAction = { [weak self] in
      self?.refreshDataSource()
    }
    navigationController?.pushViewController(addReceiptViewController, animated: true)
  }
  
  @IBAction private func onLocationsPressed(_ sender: UIBarButtonItem) {
    
    let locationsViewController = LocationsViewController.instantiate()
    locationsViewController.receipts = viewModel.displayedItems.value
    present(locationsViewController, animated: true)
  }
  
  @IBAction private func onChartPressed(_ sender: UIBarButtonItem) {
    
    let chartViewController = ChartViewController.instantiate()
    chartViewController.receipts = viewModel.displayedItems.value
    present(chartViewController, animated: true)
  }
}

// MARK: - SetupUI

extension ReceiptsViewController {
  
  private func setupTableView() {
    
    refreshControl.addTarget(self,
                             action: #selector(refreshDataSource),
                             for: .valueChanged)
    tableView.refreshControl = refreshControl
    tableView.tableFooterView = UIView()
  }
}

// MARK: - Binding

extension ReceiptsViewController {
  
  private func bindTableView() {
    
    viewModel
      .displayedItems
      .skip(1)
      .observeOn(MainScheduler.asyncInstance)
      .subscribe(onNext: { [weak self] displayedItems in
        
        guard let self = self else { return }
        
        self.tableView.reloadData()
        self.noReceiptsLabel.isHidden = !displayedItems.isEmpty
      }).disposed(by: disposeBag)
  }
}

// MARK: - Behavior

extension ReceiptsViewController {
  
  @objc
  private func refreshDataSource() {
    
    loadingOverlay.show(onTopOf: view)
    viewModel.refresh { [weak self] error in
      
      self?.loadingOverlay.hide()
      if let error = error {
        GrowingNotificationBanner.receiptsFetchFailed(withError: error).show()
      }
      DispatchQueue.main.async {
        self?.tableView.refreshControl?.endRefreshing()
      }
    }
  }
}

// MARK: - UITableViewDataSource

extension ReceiptsViewController: UITableViewDataSource {
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    viewModel.displayedItems.value.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    let cell: ReceiptTableViewCell = tableView.dequeueCell(for: indexPath)
    
    let receipt = viewModel.displayedItems.value[indexPath.row]
    cell.setup(paymentTerm: receipt.paymentTerm, value: receipt.value)

    return cell
  }
}

// MARK: - UITableViewDelegate

extension ReceiptsViewController: UITableViewDelegate {

  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
    tableView.deselectRow(at: indexPath, animated: true)
    let receiptDetailsViewController = ReceiptDetailsViewController.instantiate()
    receiptDetailsViewController.currentReceipt = viewModel.displayedItems.value[indexPath.row]
    receiptDetailsViewController.onEditAction = { [weak self] in
      self?.refreshDataSource()
    }
    navigationController?.pushViewController(receiptDetailsViewController, animated: true)
  }
  
  func tableView(_ tableView: UITableView,
                 commit editingStyle: UITableViewCell.EditingStyle,
                 forRowAt indexPath: IndexPath) {
    
    if let receiptId = viewModel.displayedItems.value[indexPath.row].id, editingStyle == .delete {
      
      viewModel.deleteReceipt(withId: receiptId) { error in
        if let error = error {
          GrowingNotificationBanner.receiptsDeletionFailed(withError: error).show()
        } else {
          tableView.deleteRows(at: [indexPath], with: .fade)
        }
      }
    }
  }
}
