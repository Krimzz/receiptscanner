//
//  LocationsViewController.swift
//  ReceiptScanner
//
//  Created by Andrei Budisan on 02/06/2020.
//  Copyright © 2020 Andrei Budisan. All rights reserved.
//

import UIKit
import MapKit
import ViewModel
import RxRelay
import RxSwift
import Model

class LocationsViewController: UIViewController {
  
  @IBOutlet private weak var mapView: MKMapView!
  
  public var receipts: [Receipt]?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    if let receipts = receipts, !receipts.isEmpty {
      configureMapView(for: receipts)
    }
  }
}

// MARK: - Button Action

extension LocationsViewController {
  
  @IBAction private func onCancelPressed(_ sender: Any) {
    
    dismiss(animated: true)
  }
}

// MARK: - SetupUI

extension LocationsViewController {
  
  func configureMapView(for receipts: [Receipt]) {
    
    let annotations = receipts.map { receipt -> MKPointAnnotation in
      
      let annotation = MKPointAnnotation()
      
      let dateFormatter = DateFormatter()
      dateFormatter.dateStyle = .short
      let paymentDate = dateFormatter.string(from: receipt.paymentTerm)
      
      annotation.title = "Date: \(paymentDate)"
      annotation.subtitle = "Value: \(receipt.value)"
      annotation.coordinate = receipt.location
      
      return annotation
    }
    
    let receiptsCoordinates = receipts.map(\.location)
    let centerOfReceipts = centerOfCoordinates(receiptsCoordinates)
    
    mapView.setCenter(centerOfReceipts, animated: true)
    mapView.addAnnotations(annotations)
    mapView.fitMapViewToAnnotaionList()
  }
  
  func centerOfCoordinates(_ coordonates: [CLLocationCoordinate2D]) -> CLLocationCoordinate2D {
    
    var xCenter = 0.0
    var yCenter = 0.0
    var zCenter = 0.0
    
    coordonates.forEach {
      
      let latRadians = radiansFrom($0.latitude)
      let lngRadians = radiansFrom($0.longitude)

      xCenter += cos(latRadians) * cos(lngRadians)
      yCenter += cos(latRadians) * sin(lngRadians)
      zCenter += sin(latRadians)
    }
    
    xCenter /= Double(coordonates.count)
    yCenter /= Double(coordonates.count)
    zCenter /= Double(coordonates.count)
    
    let lngCenterRadians = atan2(yCenter, xCenter)
    let hyp = sqrt(pow(xCenter, 2) + pow(yCenter, 2))
    let latCenterRadians = atan2(zCenter, hyp)
    
    return CLLocationCoordinate2D(latitude: degreesFrom(latCenterRadians),
                                  longitude: degreesFrom(lngCenterRadians))
  }
  
  func radiansFrom(_ degrees: CLLocationDegrees) -> Double {
    
    degrees * .pi / 180
  }
  
  func degreesFrom(_ radians: Double) -> CLLocationDegrees {
    
    radians * 180 / .pi
  }
}

extension MKMapView {
  
  func fitMapViewToAnnotaionList() {
    
    let mapEdgePadding = UIEdgeInsets(top: 100, left: 100, bottom: 100, right: 100)
    var zoomRect = MKMapRect.null
    
    annotations.forEach {
      let aPoint = MKMapPoint($0.coordinate)
      let rect = MKMapRect(x: aPoint.x, y: aPoint.y, width: 0.1, height: 0.1)
      
      if zoomRect.isNull {
        zoomRect = rect
      } else {
        zoomRect = zoomRect.union(rect)
      }
    }
    self.setVisibleMapRect(zoomRect, edgePadding: mapEdgePadding, animated: true)
  }
}
