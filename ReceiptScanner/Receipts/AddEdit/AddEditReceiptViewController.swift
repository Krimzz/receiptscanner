//
//  AddReceiptViewController.swift
//  ReceiptScanner
//
//  Created by Andrei Budisan on 08/05/2020.
//  Copyright © 2020 Andrei Budisan. All rights reserved.
//

import UIKit
import MapKit
import ViewModel
import Model
import NotificationBannerSwift

class AddEditReceiptViewController: UIViewController {
  
  @IBOutlet private weak var mapView: MKMapView!
  @IBOutlet private weak var valueTextField: CustomTextField!
  @IBOutlet private weak var datePicker: UIDatePicker!
  
  private let locationManager = CLLocationManager()
  private var receiptLocation = CLLocationCoordinate2D()
  private var loadingOverlay = LoadingOverlay()
  
  var currentReceipt: Receipt?
  var onAddAction: (() -> Void)?
  var onEditAction: ((Receipt) -> Void)?
  
  private let receiptViewModel = ViewModelFactory.makeReceiptViewModel()
  private let authenticationViewModel = ViewModelFactory.makeAuthenticationViewModel()

  override func viewDidLoad() {
    
    super.viewDidLoad()
    
    locationManager.delegate = self
    datePicker.datePickerMode = .date
    
    setNavigationBar()
    setupTextFieldType()
    
    if currentReceipt != nil {
      setupForEditMode()
    }
  }
}

// MARK: - Button Action

extension AddEditReceiptViewController {
  
  @IBAction private func onSavePressed(_ sender: UIButton) {
    
    guard let value = valueTextField.text, !value.isEmpty else {
      valueTextField.displayErrorMessage("Please enter a valid receipt value")
      return
    }
    
    guard let owner = authenticationViewModel.currentUserId else { return }
    
    let receipt = Receipt(id: currentReceipt?.id ?? nil,
                          owner: owner,
                          value: (value as NSString).doubleValue,
                          paymentTerm: datePicker.date,
                          location: mapView.centerCoordinate)
    
    loadingOverlay.show(onTopOf: view)
    
    if currentReceipt != nil {
      receiptViewModel.update(receipt: receipt) { error in
        
        self.loadingOverlay.hide()
        if let error = error {
          GrowingNotificationBanner.receiptsUpdateFailed(withError: error).show()
        } else {
          self.onEditAction?(receipt)
          self.navigationController?.popViewController(animated: true)
        }
      }
    } else {
      receiptViewModel.add(receipt: receipt) { error in
        
        self.loadingOverlay.hide()
        if let error = error {
          GrowingNotificationBanner.receiptsAddFailed(withError: error).show()
        } else {
          self.onAddAction?()
          self.navigationController?.popViewController(animated: true)
        }
      }
    }
  }
  
  @IBAction private func onCenterLocationPressed(_ sender: UIButton) {
    
    if CLLocationManager.authorizationStatus() == .denied {
      showAlertForLocationPermissions()
    } else {
      centerMap(on: receiptLocation)
    }
  }
  
  @objc
  private func onCameraButtonPressed(_ sender: UIBarButtonItem) {
    
    let imagePicker = RSImagePickerViewController()
    imagePicker.pickerDelegate = self
    
    let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
    
    let cameraAction = UIAlertAction(title: "Camera", style: .default) { [weak self] _ in
      
      imagePicker.sourceType = .camera
      self?.present(imagePicker, animated: true)
    }
    
    let libraryAction = UIAlertAction(title: "Library", style: .default) { [weak self] _ in
      
      imagePicker.sourceType = .photoLibrary
      self?.present(imagePicker, animated: true)
    }
    
    let alertVC = UIAlertController(title: "Choose how you would like to access an image",
                                    message: nil,
                                    preferredStyle: .alert)
    alertVC.addAction(cameraAction)
    alertVC.addAction(libraryAction)
    alertVC.addAction(cancelAction)
    
    present(alertVC, animated: true)
  }
}

// MARK: - Setup UI

extension AddEditReceiptViewController {

  private func setNavigationBar() {
    
    if currentReceipt == nil {
      navigationItem.title = "Add Receipt"
    } else {
      navigationItem.title = "Edit Receipt"
    }
   
    navigationController?.navigationBar.barTintColor = UIColor(named: "Main")
    navigationController?.navigationBar.tintColor = .white
    navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.white]
    
    let cameraButton = UIBarButtonItem(barButtonSystemItem: .camera,
                                       target: self,
                                       action: #selector(onCameraButtonPressed))
    navigationItem.rightBarButtonItem = cameraButton
  }
  
  private func setupTextFieldType() {
    
    valueTextField.keyboardType = .decimalPad
    valueTextField.returnKeyType = .go
  }
  
  private func setupForEditMode() {
    
    if let currentReceipt = currentReceipt {
      centerMap(on: currentReceipt.location)
      datePicker.date = currentReceipt.paymentTerm
      valueTextField.text = String(format: "%.2f", currentReceipt.value)
    }
  }
}

// MARK: - Behavior

extension AddEditReceiptViewController {
  
  private func showAlertForLocationPermissions() {
    
    let alertController = UIAlertController(title: "Your location is not enabled!",
                                            message: """
                                                    If you wish to share your current location:
                                                    Settings -> Location
                                                    """,
                                            preferredStyle: .alert)
    
    let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in
      
      guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else { return }
      
      if UIApplication.shared.canOpenURL(settingsUrl) {
        UIApplication.shared.open(settingsUrl)
      }
    }
    
    alertController.addAction(UIAlertAction(title: "OK", style: .default))
    alertController.addAction(settingsAction)
    self.present(alertController, animated: true, completion: nil)
  }

  private func centerMap(on location: CLLocationCoordinate2D) {
    
    guard CLLocationCoordinate2DIsValid(location) else {
      log.warning("Location coordinates \(location) are invalid.")
      return
    }
    let region = MKCoordinateRegion(center: location, latitudinalMeters: 1000, longitudinalMeters: 1000)
    mapView.setRegion(region, animated: true)
  }
}

// MARK: - CLLocationManagerDelegate

extension AddEditReceiptViewController: CLLocationManagerDelegate {

  func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    
    guard let location = locations.last else { return }
    receiptLocation = location.coordinate
  }
  
  func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {

    switch status {
    case .notDetermined:
      locationManager.requestWhenInUseAuthorization()
    case .authorizedWhenInUse:
      mapView.showsUserLocation = true
      locationManager.startUpdatingLocation()
      if currentReceipt == nil {
        receiptLocation = locationManager.location?.coordinate ?? CLLocationCoordinate2D()
        centerMap(on: receiptLocation)
      }
    default:
      break
    }
  }
}

// MARK: - ImagePickerDelegate

extension AddEditReceiptViewController: ImagePickerDelegate {
  
  func pickedImage(location: URL) {
    
    if var image = try? UIImage(withContentsOfUrl: location) {
      
      if image.isExceedingLimit {
        image = OpenCVWrapper.resize(image)
      }
      
      guard let processedImage = OpenCVWrapper.prepareImage(forOCR: image) else {
        log.error("Could not get processedImage from OpenCV")
        return
      }
      
      loadingOverlay.show(onTopOf: view)
      receiptViewModel.processReceiptImage(imageBase64: processedImage.base64, with: processedImage.size) { result in
        
        self.loadingOverlay.hide()
        
        switch result {
        case .success(let result):
          
          self.valueTextField.text = result.0?.description
          self.datePicker.date = result.1
          
        case .failure(let error):
          GrowingNotificationBanner.receiptsImageProcessingFailed(withError: error.userFriendlyDescription).show()
        }
      }
    }
  }
}
