//
//  XAxisDateFormatter.swift
//  ReceiptScanner
//
//  Created by Andrei Budisan on 04/06/2020.
//  Copyright © 2020 Andrei Budisan. All rights reserved.
//

import Foundation
import Charts

class XAxisDateFormatter: NSObject, IAxisValueFormatter {
  private let dateFormatter = DateFormatter()
  
  override init() {
    super.init()
    
    dateFormatter.dateStyle = .short
  }
  
  public func stringForValue(_ value: Double, axis: AxisBase?) -> String {
    
    dateFormatter.string(from: Date(timeIntervalSince1970: value))
  }
}
