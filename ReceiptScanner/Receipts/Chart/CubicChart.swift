//
//  CubicChart.swift
//  ReceiptScanner
//
//  Created by Andrei Budisan on 03/06/2020.
//  Copyright © 2020 Andrei Budisan. All rights reserved.
//

import UIKit
import Charts
import Model

class CubicChart: UIView {
  
  let lineChartView = LineChartView()
  var lineDataEntry = [ChartDataEntry]()
}

// MARK: - SetupUI

extension CubicChart {
  
  func chartSetup(for receipts: [Receipt]) {
    
    self.backgroundColor = .white
    self.addSubview(lineChartView)
    
    lineChartView.translatesAutoresizingMaskIntoConstraints = false
    lineChartView.topAnchor.constraint(equalTo: self.topAnchor, constant: 20).isActive = true
    lineChartView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
    lineChartView.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
    lineChartView.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
    
    lineChartView.animate(xAxisDuration: 2.0, yAxisDuration: 2.0, easingOption: .easeInSine)
    
    add(receipts: receipts)
  }
  
  func add(receipts: [Receipt]) {
    
    lineChartView.noDataTextColor = .black
    lineChartView.noDataText = "No data for the chart."
    lineChartView.backgroundColor = .white
    
    receipts.forEach {
      let dataPoint = ChartDataEntry(x: $0.paymentTerm.timeIntervalSince1970, y: $0.value)
      lineDataEntry.append(dataPoint)
    }
    
    let chartDataSet = LineChartDataSet(entries: lineDataEntry, label: "Consumption")
    
    let chartData = LineChartData()
    chartData.addDataSet(chartDataSet)
    chartData.setDrawValues(true)
    
    if let mainColor = UIColor(named: "Main") {
      chartDataSet.colors = [mainColor]
      chartDataSet.setCircleColor(mainColor)
      chartDataSet.circleHoleColor = mainColor
      
      chartDataSet.circleRadius = 4.0
      chartDataSet.cubicIntensity = 0.2
      
      chartDataSet.drawHorizontalHighlightIndicatorEnabled = false
      chartDataSet.drawVerticalHighlightIndicatorEnabled = false
      
      let gradientColors = [mainColor.cgColor, UIColor.clear.cgColor] as CFArray
      let colorLocations: [CGFloat] = [1.0, 0.0]
      
      guard let gradient = CGGradient(colorsSpace: CGColorSpaceCreateDeviceRGB(),
                                      colors: gradientColors,
                                      locations: colorLocations) else {
        
        log.error("Gradient error on creating chart")
        return
      }
      
      chartDataSet.fill = Fill.fillWithLinearGradient(gradient, angle: 90.0)
      chartDataSet.drawFilledEnabled = true
      
      lineChartView.xAxis.labelPosition = .bottom
      lineChartView.xAxis.drawGridLinesEnabled = false
      lineChartView.xAxis.valueFormatter = XAxisDateFormatter()
      lineChartView.chartDescription?.enabled = false
      lineChartView.legend.enabled = true
      lineChartView.rightAxis.enabled = false
      lineChartView.leftAxis.drawGridLinesEnabled = false
      lineChartView.leftAxis.drawLabelsEnabled = true
      
      lineChartView.data = chartData
    }
  }
}
