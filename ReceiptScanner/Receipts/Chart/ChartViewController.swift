//
//  ChartViewController.swift
//  ReceiptScanner
//
//  Created by Andrei Budisan on 03/06/2020.
//  Copyright © 2020 Andrei Budisan. All rights reserved.
//

import UIKit
import Model

class ChartViewController: UIViewController {
  
  @IBOutlet private weak var chartView: CubicChart!
  @IBOutlet private weak var totalValueLabel: UILabel!
  
  public var receipts: [Receipt]?
  
  override func viewDidLoad() {
    super.viewDidLoad()

    if let receipts = receipts, !receipts.isEmpty {
      setupTotalLabel(for: receipts)
      chartView.chartSetup(for: receipts)
    }
  }
}

// MARK: - Button Action

extension ChartViewController {
  
  @IBAction private func onCancelPressed(_ sender: UIButton) {
    
    dismiss(animated: true)
  }
}

// MARK: - SetupUI

extension ChartViewController {
  
  func setupTotalLabel(for receipts: [Receipt]) {
    
    let totalValue = receipts.map(\.value).reduce(0, +)
    totalValueLabel.text = "Total: " + String(format: "%.2f", totalValue)
  }
}
