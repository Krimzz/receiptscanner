//
//  ReceiptTableViewCell.swift
//  ReceiptScanner
//
//  Created by Andrei Budisan on 06/05/2020.
//  Copyright © 2020 Andrei Budisan. All rights reserved.
//

import UIKit

class ReceiptTableViewCell: UITableViewCell {
  
  @IBOutlet private weak var dateLabel: UILabel!
  @IBOutlet private weak var valueLabel: UILabel!
  
  func setup(paymentTerm: Date, value: Double) {
    
    let dateFormatter = DateFormatter()
    dateFormatter.locale = NSLocale.current
    dateFormatter.dateStyle = .medium
    dateFormatter.timeStyle = .short
    dateLabel.text = dateFormatter.string(from: paymentTerm)
    valueLabel.text = "\(value)"
  }
}
