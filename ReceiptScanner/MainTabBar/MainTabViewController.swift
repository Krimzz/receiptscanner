//
//  MainTabViewController.swift
//  ReceiptScanner
//
//  Created by Andrei Budisan on 26/04/2020.
//  Copyright © 2020 Andrei Budisan. All rights reserved.
//

import UIKit
import RxSwift
import RxRelay
import ViewModel
import NotificationBannerSwift

final class MainTabViewController: UITabBarController {
  
  private var viewModel = ViewModelFactory.makeAuthenticationViewModel()
  
  private let disposeBag = DisposeBag()
  
  override func viewDidAppear(_ animated: Bool) {
    
    super.viewDidAppear(animated)
    bindViewModel()
  }
  
  private func presentLoginIfNeeded() {
    
    let loginViewController = LoginViewController.instantiate()
    
    let loginNavigationController = loginViewController.navEmbedded
    if #available(iOS 13.0, *) {
      loginNavigationController.isModalInPresentation = true
    }
    present(loginNavigationController, animated: true, completion: nil)
  }
}

// MARK: - Binding

extension MainTabViewController {
  
  private func bindViewModel() {
    
    viewModel
      .onLoginStatusChanged
      .observeOn(MainScheduler.instance)
      .subscribe(onNext: { [weak self] isLogedIn in
        
        if !isLogedIn {
          self?.presentLoginIfNeeded()
        }
      })
      .disposed(by: disposeBag)
    
    viewModel
      .reachability
      .skip(1)
      .observeOn(MainScheduler.instance)
      .subscribe(onNext: { isReachable in
        
        if !isReachable {
          GrowingNotificationBanner.noInternet.show()
        }
      }).disposed(by: disposeBag)
  }
}
