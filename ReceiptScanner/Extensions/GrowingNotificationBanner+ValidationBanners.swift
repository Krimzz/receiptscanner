//
//  GrowingNotificationBanner+ValidationBanners.swift
//  ReceiptScanner
//
//  Created by Andrei Budisan on 26/04/2020.
//  Copyright © 2020 Andrei Budisan. All rights reserved.
//

import Foundation
import NotificationBannerSwift
import typealias ViewModel.ErrorMessage

extension GrowingNotificationBanner {
  
  static var noInternet: GrowingNotificationBanner {
    
    let banner = GrowingNotificationBanner(title: "No internet connection", subtitle: "", style: .danger)
    banner.applyStyling(titleTextAlign: .center)
    banner.applyStyling(subtitleTextAlign: .center)
    return banner
  }
  
  static func userProfileUpdateFailed(withError error: ErrorMessage) -> GrowingNotificationBanner {
    
    let banner = GrowingNotificationBanner(title: "Failed to update", subtitle: "", style: .warning)
    banner.applyStyling(titleTextAlign: .center)
    banner.applyStyling(subtitleTextAlign: .center)
    return banner
  }
  
  static func userAuthenticationFailed(withError error: ErrorMessage) -> GrowingNotificationBanner {
    
    let banner = GrowingNotificationBanner(title: "Authentication Failed", subtitle: error, style: .danger)
    banner.applyStyling(titleTextAlign: .center)
    banner.applyStyling(subtitleTextAlign: .center)
    return banner
  }
  
  static func userFetchFailed(withError error: ErrorMessage) -> GrowingNotificationBanner {
    
    let banner = GrowingNotificationBanner(title: "Failed to retrieve user data", subtitle: error, style: .danger)
    banner.applyStyling(titleTextAlign: .center)
    banner.applyStyling(subtitleTextAlign: .center)
    return banner
  }
  
  static func receiptsFetchFailed(withError error: ErrorMessage) -> GrowingNotificationBanner {
    
    let banner = GrowingNotificationBanner(title: "Failed to retrieve your receipts", subtitle: error, style: .danger)
    banner.applyStyling(titleTextAlign: .center)
    banner.applyStyling(subtitleTextAlign: .center)
    return banner
  }
  
  static func receiptsDeletionFailed(withError error: ErrorMessage) -> GrowingNotificationBanner {
    
    let banner = GrowingNotificationBanner(title: "Failed to delete your receipt", subtitle: error, style: .danger)
    banner.applyStyling(titleTextAlign: .center)
    banner.applyStyling(subtitleTextAlign: .center)
    return banner
  }
  
  static func receiptsAddFailed(withError error: ErrorMessage) -> GrowingNotificationBanner {
    
    let banner = GrowingNotificationBanner(title: "Failed to add a new receipt", subtitle: error, style: .danger)
    banner.applyStyling(titleTextAlign: .center)
    banner.applyStyling(subtitleTextAlign: .center)
    return banner
  }
  
  static func receiptsUpdateFailed(withError error: ErrorMessage) -> GrowingNotificationBanner {
    
    let banner = GrowingNotificationBanner(title: "Failed to update the receipt", subtitle: error, style: .danger)
    banner.applyStyling(titleTextAlign: .center)
    banner.applyStyling(subtitleTextAlign: .center)
    return banner
  }
  
  static func receiptsImageProcessingFailed(withError error: ErrorMessage) -> GrowingNotificationBanner {
    
    let banner = GrowingNotificationBanner(title: "Failed to process the receipt", subtitle: error, style: .danger)
    banner.applyStyling(titleTextAlign: .center)
    banner.applyStyling(subtitleTextAlign: .center)
    return banner
  }
}
