//
//  UIImage+Base64.swift
//  ReceiptScanner
//
//  Created by Andrei Budisan on 10/05/2020.
//  Copyright © 2020 Andrei Budisan. All rights reserved.
//

import UIKit

extension UIImage {
  
  var base64: String {
    
    self.jpegData(compressionQuality: 1)?.base64EncodedString() ?? ""
  }
  
  var isExceedingLimit: Bool {
    
    if let imageSize = self.jpegData(compressionQuality: 1)?.count, imageSize > 10000000 {
      return true
    } else {
      return false
    }
  }
  
  public convenience init?(withContentsOfUrl url: URL) throws {
    
    let imageData = try Data(contentsOf: url)
    self.init(data: imageData)
  }
}
