//
//  String+Validations.swift
//  ReceiptScanner
//
//  Created by Andrei Budisan on 27/04/2020.
//  Copyright © 2020 Andrei Budisan. All rights reserved.
//

import Foundation

extension String {
  
  func isValidName() -> Bool {

    do {
      let stringRegex = "[A-Z][a-z]+\\s[A-Z][a-z]+.*"
      let regex = try NSRegularExpression(pattern: stringRegex, options: [])
      
      return regex.matches(in: self, options: [], range: NSRange(location: 0, length: self.count)).count == 1
    } catch {
      return false
    }
  }
  
  func isValidEmail() -> Bool {
    
    do {
      let stringRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
      let regex = try NSRegularExpression(pattern: stringRegex, options: .caseInsensitive)
      
      return regex.matches(in: self, options: [], range: NSRange(location: 0, length: self.count)).count == 1
    } catch {
      return false
    }
  }
  
  func isValidPassword() -> Bool {
    
    self.count >= 8
  }
}
