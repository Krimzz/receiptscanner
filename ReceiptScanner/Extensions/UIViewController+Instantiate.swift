//
//  UIViewController+Instantiate.swift
//  ReceiptScanner
//
//  Created by Andrei Budisan on 27/04/2020.
//  Copyright © 2020 Andrei Budisan. All rights reserved.
//

import UIKit

extension UIViewController {

  static var storyboard: UIStoryboard {

    UIStoryboard(name: "\(self)", bundle: Bundle(for: self))
  }

  class func instantiate() -> Self {

    viewController(viewControllerClass: self)
  }

  var navEmbedded: UINavigationController {

    UINavigationController(rootViewController: self)
  }

  private static func viewController<T: UIViewController>(viewControllerClass: T.Type) -> T {

    guard let scene = storyboard.instantiateInitialViewController() as? T
      else { fatalError("Could not find storyboard named: \(self) with initial view controller set.") }

    return scene
  }
}
