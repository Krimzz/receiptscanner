//
//  UIViewController+Keyboard.swift
//  ReceiptScanner
//
//  Created by Andrei Budisan on 27/04/2020.
//  Copyright © 2020 Andrei Budisan. All rights reserved.
//

import UIKit

extension UIViewController {
  
  func hideKeyboardWhenTappedAround() {
    
    let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self,
                                                             action: #selector(UIViewController.dismissKeyboard))
    tap.cancelsTouchesInView = false
    view.addGestureRecognizer(tap)
  }
  
  @objc func dismissKeyboard() {
    
    view.endEditing(true)
  }
}
