//
//  UITableView+Convenience.swift
//  ReceiptScanner
//
//  Created by Andrei Budisan on 07/05/2020.
//  Copyright © 2020 Andrei Budisan. All rights reserved.
//

import UIKit

extension UITableView {
  
  func register<T>(cell: T.Type) where T: UITableViewCell {
    
    let nib = UINib(nibName: "\(cell.self)", bundle: Bundle(for: T.self))
    register(nib, forCellReuseIdentifier: cell.identifier)
  }
  
  func dequeueCell<T: UITableViewCell>(for indexPath: IndexPath) -> T {

    guard let cell = dequeueReusableCell(withIdentifier: T.className, for: indexPath) as? T
      else { fatalError("Failed dequeueing cell with identifier: \(T.className)") }
    return cell
  }
}

extension UITableViewCell {
  
  fileprivate static var identifier: String { "\(self)" }
}

extension UIView {
  
  class var className: String {
    String(describing: self)
  }
}
