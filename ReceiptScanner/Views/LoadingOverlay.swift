//
//  LoadingOverlay.swift
//  ReceiptScanner
//
//  Created by Andrei Budisan on 28/04/2020.
//  Copyright © 2020 Andrei Budisan. All rights reserved.
//

import UIKit

final class LoadingOverlay: DesignableView {
  
  @IBOutlet private var contentView: UIView!
  @IBOutlet private weak var activityIndicator: UIActivityIndicatorView!
  
  override init(frame: CGRect) {

    super.init(frame: frame)
    commonInit()
  }

  required init?(coder aDecoder: NSCoder) {

    super.init(coder: aDecoder)
    commonInit()
  }

  private func commonInit() {

    contentView.frame = self.bounds
    contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]

    addSubview(contentView)
  }

  func show(onTopOf view: UIView) {
    guard !view.subviews.contains(self) else { return }
    frame = view.bounds
    view.addSubview(self)
    activityIndicator.startAnimating()
  }

  func hide() {
    
    DispatchQueue.main.async {
      self.activityIndicator.stopAnimating()
      self.removeFromSuperview()
    }
  }
}
