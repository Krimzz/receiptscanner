//
//  CustomTextField.swift
//  ReceiptScanner
//
//  Created by Andrei Budisan on 26/04/2020.
//  Copyright © 2020 Andrei Budisan. All rights reserved.
//

import UIKit

class CustomTextField: DesignableView {
  
  @IBOutlet private weak var iconImageView: UIImageView!
  @IBOutlet private weak var inputTextField: UITextField!
  @IBOutlet private weak var errorLabel: UILabel!
  
  var onDidBeginEditing: ((CustomTextField) -> Void)?
  var onDidEndEditing: ((CustomTextField) -> Void)?
  var onShouldReturn: ((CustomTextField) -> Bool)?
  
  @IBInspectable
  var isIconHidden: Bool {
    get {
      iconImageView.isHidden
    }
    set {
      iconImageView.isHidden = newValue
    }
  }
  
  @IBInspectable
  var icon: UIImage? {
    get {
      iconImageView.image
    }
    set {
      iconImageView.image = newValue
    }
  }
  
  @IBInspectable
  var text: String? {
    get {
      inputTextField.text
    }
    set {
      inputTextField.text = newValue
    }
  }
  
  var keyboardType: UIKeyboardType {
    get {
      inputTextField.keyboardType
    }
    set {
      inputTextField.keyboardType = newValue
    }
  }
  
  @IBInspectable
  var textColor: UIColor? {
    get {
      inputTextField.textColor
    }
    set {
      inputTextField.textColor = newValue
    }
  }
  
  var capitalizeType: UITextAutocapitalizationType {
    get {
      inputTextField.autocapitalizationType
    }
    set {
      inputTextField.autocapitalizationType = newValue
    }
  }
  
  @IBInspectable
  var placeholderText: String? {
    get {
      inputTextField.placeholder
    }
    set {
      inputTextField.placeholder = newValue
    }
  }
  
  @IBInspectable
  public var secureTextEntry: Bool {
    get {
      inputTextField.isSecureTextEntry
    }
    set {
      inputTextField.isSecureTextEntry = newValue
    }
  }
  
  @IBInspectable
  public var errorLabelText: String? {
    get {
      errorLabel.text
    }
    set {
      errorLabel.text = newValue
    }
  }
  
  public var returnKeyType: UIReturnKeyType {
    get {
      inputTextField.returnKeyType
    }
    set {
      inputTextField.returnKeyType = newValue
    }
  }
  
  override func becomeFirstResponder() -> Bool {
    
    inputTextField.becomeFirstResponder()
  }
  
  override func resignFirstResponder() -> Bool {
    
    inputTextField.resignFirstResponder()
  }
  
  func displayErrorMessage(_ errorMessage: String) {
    
    if errorMessage != "" {
      errorLabel.text = errorMessage
      inputTextField.textColor = .red
    } else {
      errorLabel.text = errorMessage
      inputTextField.textColor = .black
    }
  }
  
  override func awakeFromNib() {
    
    super.awakeFromNib()
    inputTextField.delegate = self
  }
}

extension CustomTextField: UITextFieldDelegate {
  
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    
    onShouldReturn?(self) ?? true
  }
  
  func textFieldDidBeginEditing(_ textField: UITextField) {
    
    onDidBeginEditing?(self)
  }
  
  func textFieldDidEndEditing(_ textField: UITextField) {
    
    onDidEndEditing?(self)
  }
}
