//
//  CustomKeyboardScrollView.swift
//  ReceiptScanner
//
//  Created by Andrei Budisan on 26/04/2020.
//  Copyright © 2020 Andrei Budisan. All rights reserved.
//

import UIKit

@IBDesignable
class CustomKeyboardScrollView: UIScrollView {
  
  override func awakeFromNib() {
    
    super.awakeFromNib()
    addKeyboardObservers()
  }

  func addKeyboardObservers() {
    
    NotificationCenter.default.addObserver(self,
                                           selector: #selector(keyboardWillAppear(notification:)),
                                           name: UIResponder.keyboardDidShowNotification, object: nil)
    NotificationCenter.default.addObserver(self,
                                           selector: #selector(keyboardWillDisappear(notification:)),
                                           name: UIResponder.keyboardDidHideNotification, object: nil)
  }
  
  @objc func keyboardWillAppear(notification: Notification) {
    
    guard let userInfo = notification.userInfo,
      let frame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size else { return }
    let contentInset = UIEdgeInsets(top: 0, left: 0, bottom: frame.height, right: 0)
    self.contentInset = contentInset
    self.scrollIndicatorInsets = contentInset
  }
  
  @objc func keyboardWillDisappear(notification: Notification) {
    
    let contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    self.contentInset = contentInset
    self.scrollIndicatorInsets = contentInset
  }
}
