//
//  DesignableView.swift
//  ReceiptScanner
//
//  Created by Andrei Budisan on 26/04/2020.
//  Copyright © 2020 Andrei Budisan. All rights reserved.
//

import UIKit

@IBDesignable
class DesignableView: UIView {

  override init(frame: CGRect) {
    super.init(frame: frame)
    setup()
  }

  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setup()
  }

  func setup() {

    let nib = UINib(nibName: String(describing: type(of: self)),
                    bundle: Bundle(for: type(of: self)))
    
    guard let view = nib.instantiate(withOwner: self, options: nil).first as? UIView
      else { fatalError("Could not load nib for \(String(describing: type(of: self)))") }
    
    view.frame = bounds
    view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    addSubview(view)
  }
}
