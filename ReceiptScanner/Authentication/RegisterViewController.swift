//
//  RegisterViewController.swift
//  ReceiptScanner
//
//  Created by Andrei Budisan on 26/04/2020.
//  Copyright © 2020 Andrei Budisan. All rights reserved.
//

import UIKit
import ViewModel
import NotificationBannerSwift

class RegisterViewController: UIViewController {
  
  @IBOutlet private weak var emailTextField: CustomTextField!
  @IBOutlet private weak var fullNameTextField: CustomTextField!
  @IBOutlet private weak var passwordTextField: CustomTextField!
  @IBOutlet private weak var confirmPasswordTextField: CustomTextField!
  
  private var loadingOverlay = LoadingOverlay()
  private var authenticationViewModel = ViewModelFactory.makeAuthenticationViewModel()
  
  override func viewDidLoad() {
    
    super.viewDidLoad()
    initTextFields()
    hideKeyboardWhenTappedAround()
    setupTextFieldsReturnKeyType()
    setTextFieldDidBeginEditing()
    setTextFieldDidEndEditing()
    setTextFieldShouldReturn()
  }
}

// MARK: - SetupUI

extension RegisterViewController {
  
  private func initTextFields() {
    
    fullNameTextField.text = ""
    emailTextField.text = ""
    passwordTextField.text = ""
    confirmPasswordTextField.text = ""

    fullNameTextField.displayErrorMessage("")
    emailTextField.displayErrorMessage("")
    passwordTextField.displayErrorMessage("")
    confirmPasswordTextField.displayErrorMessage("")
    
    fullNameTextField.capitalizeType = .words
  }
  
  private func setupTextFieldsReturnKeyType() {
    
    fullNameTextField.returnKeyType = .next
    emailTextField.returnKeyType = .next
    passwordTextField.returnKeyType = .next
    confirmPasswordTextField.returnKeyType = .go
  }
}

// MARK: - Behavior

extension RegisterViewController {
  
  private func register() {

    let fullName = fullNameTextField.text ?? ""
    let email = emailTextField.text ?? ""
    let password = passwordTextField.text ?? ""
    let confirmPassword = confirmPasswordTextField.text ?? ""

    if isValid(fullName: fullName, email: email, password: password, confirmPassword: confirmPassword) {
      registerUser(fullName: fullName, email: email, password: password)
    }
  }
  
  private func registerUser(fullName: String, email: String, password: String) {

    loadingOverlay.show(onTopOf: view)
    authenticationViewModel.registerUser(fullName: fullName, email: email, password: password) { error in
      
      self.loadingOverlay.hide()
      if let error = error {
        GrowingNotificationBanner.userAuthenticationFailed(withError: error).show()
      } else {
        self.dismiss(animated: true)
      }
    }
  }

  private func isValid(fullName: String, email: String, password: String, confirmPassword: String) -> Bool {

    var valid = true

    if !fullName.isValidName() {
      valid = false
      if fullName.isEmpty {
        fullNameTextField.displayErrorMessage("The fullname field is empty")
      } else {
        fullNameTextField.displayErrorMessage("Name must start with capital letter")
      }
    }

    if !email.isValidEmail() {
      valid = false
      if email.isEmpty {
        emailTextField.displayErrorMessage("The email field is empty")
      } else {
        emailTextField.displayErrorMessage("Email format is not correct")
      }
    }

    if !password.isValidPassword() {
      valid = false
      if password.isEmpty {
        passwordTextField.displayErrorMessage("The password field is empty")
      } else {
        passwordTextField.displayErrorMessage("Password should have at least 8 characters")
      }
    }

    if confirmPassword.isEmpty || confirmPassword != password {
      valid = false
        confirmPasswordTextField.displayErrorMessage("Password mismatch")
    }

    return valid
  }
}

// MARK: - Button Action

extension RegisterViewController {
  
  @IBAction private func onRegisterPressed(_ sender: UIButton) {
    
    register()
  }
  
  @IBAction private func onCancelPressed(_ sender: UIButton) {
    
    navigationController?.popViewController(animated: true)
  }
}

// MARK: - UITextFieldDelegate + Validations

extension RegisterViewController: UITextFieldDelegate {
  
  private func setTextFieldDidBeginEditing() {
    
    fullNameTextField.onDidBeginEditing = { [weak self] _ in
      
      self?.fullNameTextField.textColor = .black
      self?.fullNameTextField.displayErrorMessage("")
    }
    
    emailTextField.onDidBeginEditing = { [weak self] _ in
      
      self?.emailTextField.textColor = .black
      self?.emailTextField.displayErrorMessage("")
    }
    
    passwordTextField.onDidBeginEditing = { [weak self] _ in
      
      self?.passwordTextField.textColor = .black
      self?.passwordTextField.displayErrorMessage("")
    }
    
    confirmPasswordTextField.onDidBeginEditing = { [weak self] _ in
      
      self?.confirmPasswordTextField.textColor = .black
      self?.confirmPasswordTextField.displayErrorMessage("")
    }
  }
  
  private func setTextFieldDidEndEditing() {
    
    fullNameTextField.onDidEndEditing = { [weak self] _ in
      
      let fullName = self?.fullNameTextField.text ?? ""
      if !fullName.isValidName() {
        self?.fullNameTextField.displayErrorMessage("Name must start with capital letter")
      } else {
        self?.fullNameTextField.displayErrorMessage("")
      }
    }
    
    emailTextField.onDidEndEditing = { [weak self] _ in
      
      let email = self?.emailTextField.text ?? ""
      if !email.isValidEmail() {
        self?.emailTextField.displayErrorMessage("Email format is not correct")
      } else {
        self?.emailTextField.displayErrorMessage("")
      }
    }
    
    passwordTextField.onDidEndEditing = { [weak self] _ in
      
      let password = self?.passwordTextField.text ?? ""
      if !password.isValidPassword() {
        self?.passwordTextField.displayErrorMessage("Password should have at least 8 characters")
      } else {
        self?.passwordTextField.displayErrorMessage("")
      }
    }
    
    confirmPasswordTextField.onDidEndEditing = { [weak self] _ in
      
      let confirmPassword = self?.confirmPasswordTextField.text ?? ""
      if confirmPassword != (self?.passwordTextField.text ?? "") {
        self?.confirmPasswordTextField.displayErrorMessage("Password mismatch")
      } else {
        self?.passwordTextField.displayErrorMessage("")
      }
    }
  }
  
  private func setTextFieldShouldReturn() {
    
    fullNameTextField.onShouldReturn = { [weak self] _ in
      
      self?.emailTextField.becomeFirstResponder() ?? true
    }
    
    emailTextField.onShouldReturn = { [weak self] _ in
      
      self?.passwordTextField.becomeFirstResponder() ?? true
    }
    
    passwordTextField.onShouldReturn = { [weak self] _ in
      
      self?.confirmPasswordTextField.becomeFirstResponder() ?? true
    }
    
    confirmPasswordTextField.onShouldReturn = { [weak self] _ in
      
      self?.view.endEditing(true)
      self?.register()
      return true
    }
  }
}
