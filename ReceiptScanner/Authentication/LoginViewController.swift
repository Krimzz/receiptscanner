//
//  LogViewController.swift
//  ReceiptScanner
//
//  Created by Andrei Budisan on 26/04/2020.
//  Copyright © 2020 Andrei Budisan. All rights reserved.
//

import UIKit
import ViewModel
import NotificationBannerSwift

class LoginViewController: UIViewController {
  
  @IBOutlet private weak var emailTextField: CustomTextField!
  @IBOutlet private weak var passwordTextField: CustomTextField!
  
  private var loadingOverlay = LoadingOverlay()
  public var authenticationViewModel = ViewModelFactory.makeAuthenticationViewModel()
  
  override func viewDidLoad() {
    
    super.viewDidLoad()
    navigationController?.isNavigationBarHidden = true
    hideKeyboardWhenTappedAround()
    setupTextFieldsReturnKeyType()
    setTextFieldDidBeginEditing()
    setTextFieldDidEndEditing()
    setTextFieldShouldReturn()
  }
  
  override func viewWillAppear(_ animated: Bool) {

    super.viewWillAppear(animated)
    initTextFields()
  }
}

// MARK: - SetupUI

extension LoginViewController {
  
  private func initTextFields() {
    
    emailTextField.text = ""
    passwordTextField.text = ""

    emailTextField.displayErrorMessage("")
    passwordTextField.displayErrorMessage("")
  }
  
  private func setupTextFieldsReturnKeyType() {
    
    emailTextField.returnKeyType = .next
    passwordTextField.returnKeyType = .go
  }
}

// MARK: - Button Action

extension LoginViewController {
  
  @IBAction private func onLoginPressed(_ sender: UIButton) {
    
    login()
  }
  
  @IBAction private func onRegisterPressed(_ sender: Any) {
    
    let registerViewController = RegisterViewController.instantiate()
    if #available(iOS 13.0, *) {
      registerViewController.isModalInPresentation = true
    }
    navigationController?.pushViewController(registerViewController, animated: true)
  }
}

// MARK: - Behavior

extension LoginViewController {
  
  private func login() {

    let email = emailTextField.text ?? ""
    let password = passwordTextField.text ?? ""

    if isValid(email: email, password: password) {
      loginUser(email: email, password: password)
    }
  }
  
  public func loginUser(email: String, password: String) {
    
    loadingOverlay.show(onTopOf: view)
    authenticationViewModel.logInUser(email: email, password: password) { error in
      
      self.loadingOverlay.hide()
      
      if let error = error {
        GrowingNotificationBanner.userAuthenticationFailed(withError: error).show()
        self.emailTextField.displayErrorMessage(" ")
        self.passwordTextField.displayErrorMessage(" ")
      } else {
        self.dismiss(animated: true)
      }
    }
  }
  
  private func isValid(email: String, password: String) -> Bool {

    var valid = true

    if !email.isValidEmail() {
      valid = false
      if email.isEmpty {
        emailTextField.displayErrorMessage("The email field is empty")
      } else {
        emailTextField.displayErrorMessage("Email format is not correct")
      }
    }

    if !password.isValidPassword() {
      valid = false
      if password.isEmpty {
        passwordTextField.displayErrorMessage("The password field is empty")
      } else {
        passwordTextField.displayErrorMessage("Password should have at least 8 characters")
      }
    }

    return valid
  }
}

// MARK: - UITextFieldDelegate + TextField Validations

extension LoginViewController: UITextFieldDelegate {

  func setTextFieldDidBeginEditing() {

    emailTextField.onDidBeginEditing = { [weak self] _ in
      
      self?.emailTextField.textColor = .black
      self?.emailTextField.displayErrorMessage("")
    }

    passwordTextField.onDidBeginEditing = { [weak self] _ in

      self?.passwordTextField.textColor = .black
      self?.passwordTextField.displayErrorMessage("")
    }
  }

  func setTextFieldDidEndEditing() {

    emailTextField.onDidEndEditing = { [weak self] _ in

      let email = self?.emailTextField.text ?? ""
      if !email.isValidEmail() {
        self?.emailTextField.displayErrorMessage("Email format is not correct")
      } else {
        self?.emailTextField.displayErrorMessage("")
      }
    }

    passwordTextField.onDidEndEditing = { [weak self] _ in

      let password = self?.passwordTextField.text ?? ""
      if !password.isValidPassword() {
        self?.passwordTextField.displayErrorMessage("Password should have at least 8 characters")
      } else {
        self?.passwordTextField.displayErrorMessage("")
      }
    }
  }

  func setTextFieldShouldReturn() {

    emailTextField.onShouldReturn = { [weak self] _ in

      self?.passwordTextField.becomeFirstResponder() ?? true
    }

    passwordTextField.onShouldReturn = { [weak self] _ in

      self?.view.endEditing(true)
      self?.login()
      return true
    }
  }
}
