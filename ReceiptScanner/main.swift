//
//  main.swift
//  ReceiptScanner
//
//  Created by Andrei Budisan on 07/03/2020.
//  Copyright © 2020 Andrei Budisan. All rights reserved.
//

import Foundation
import UIKit
import Log

var log = Log.log

UIApplicationMain(CommandLine.argc, CommandLine.unsafeArgv, nil, NSStringFromClass(AppDelegate.self))
