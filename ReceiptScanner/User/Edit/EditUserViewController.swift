//
//  EditUserViewController.swift
//  ReceiptScanner
//
//  Created by Andrei Budisan on 29/04/2020.
//  Copyright © 2020 Andrei Budisan. All rights reserved.
//

import UIKit
import ViewModel
import NotificationBannerSwift

protocol EditUserDelegate: class {
  
  func didUpdateUser(with name: String)
}

class EditUserViewController: UIViewController {
  
  @IBOutlet private weak var fullNameTextField: CustomTextField!
  @IBOutlet private weak var passwordTextField: CustomTextField!
  @IBOutlet private weak var confirmPasswordTextField: CustomTextField!
  
  private let userViewModel = ViewModelFactory.makeUserViewModel()
  private let authenticationViewModel = ViewModelFactory.makeAuthenticationViewModel()
  
  var fullName: String?
  var loadingOverlay = LoadingOverlay()
  weak var editDelegate: EditUserDelegate?
  
  override func viewDidLoad() {
    
    super.viewDidLoad()
    setNavigationBar()
    setupTextFieldsReturnKeyType()
    setUpTextfieldText()
    setTextFieldDidBeginEditing()
    setTextFieldDidEndEditing()
    setTextFieldShouldReturn()
  }
}

// MARK: - Button Action

extension EditUserViewController {
  
  @IBAction private func onUpdatePressed(_ sender: UIButton) {
    
    updateUser()
  }
}

// MARK: - Setup UI

extension EditUserViewController {
  
  private func setUpTextfieldText() {
    
    guard let fullName = fullName else { return }
    fullNameTextField.text = fullName
  }
  
  private func setNavigationBar() {
    
    navigationItem.title = "Edit Profile"
    navigationController?.navigationBar.barTintColor = UIColor(named: "Main")
    navigationController?.navigationBar.tintColor = .white
    navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.white]
  }
  
  private func setupTextFieldsReturnKeyType() {
    
    fullNameTextField.returnKeyType = .next
    passwordTextField.returnKeyType = .next
    confirmPasswordTextField.returnKeyType = .go
  }
}

// MARK: - Behavior

extension EditUserViewController {
  
  private func updateUser() {
    
    let fullName = fullNameTextField.text ?? ""
    let password = passwordTextField.text ?? ""
    let confirmPassword = confirmPasswordTextField.text ?? ""
    
    guard let uid = authenticationViewModel.currentUserId else { return }
    
    if verifyValidInputs(fullName: fullName,
                         password: password,
                         confirmPassword: confirmPassword) {
      
      loadingOverlay.show(onTopOf: view)
      
      userViewModel.update(uid: uid,
                           name: fullName,
                           password: password) { error in
          
        self.loadingOverlay.hide()
        if let error = error {
          GrowingNotificationBanner.userProfileUpdateFailed(withError: error).show()
        } else {
          self.editDelegate?.didUpdateUser(with: fullName)
          self.navigationController?.popViewController(animated: true)
        }
      }
    }
  }
  
  private func verifyValidInputs(fullName: String,
                                 password: String,
                                 confirmPassword: String) -> Bool {
    var valid = true
    
    if !fullName.isValidName() && !fullName.isEmpty {
      valid = false
      fullNameTextField.displayErrorMessage("First name and last name must start with capital letter!")
    }

    if !password.isValidPassword() && !password.isEmpty {
      valid = false
      passwordTextField.displayErrorMessage("Password should have at least 6 characters!")
    }
    
    if confirmPassword != password {
      valid = false
      confirmPasswordTextField.displayErrorMessage("Password mismatch")
    }
    return valid
  }
}

// MARK: - UITextFieldDelegate + Validations

extension EditUserViewController: UITextFieldDelegate {
  
  private func setTextFieldDidBeginEditing() {
    
    fullNameTextField.onDidBeginEditing = { [weak self] _ in
      
      self?.fullNameTextField.textColor = .black
      self?.fullNameTextField.displayErrorMessage("")
    }
    
    passwordTextField.onDidBeginEditing = { [weak self] _ in
      
      self?.passwordTextField.textColor = .black
      self?.passwordTextField.displayErrorMessage("")
    }
    
    confirmPasswordTextField.onDidBeginEditing = { [weak self] _ in
      
      self?.confirmPasswordTextField.textColor = .black
      self?.confirmPasswordTextField.displayErrorMessage("")
    }
  }
  
  private func setTextFieldDidEndEditing() {
    
    fullNameTextField.onDidEndEditing = { [weak self] _ in
      
      let fullName = self?.fullNameTextField.text ?? ""
      if !fullName.isValidName() {
        self?.fullNameTextField.displayErrorMessage("Name must start with capital letter")
      } else {
        self?.fullNameTextField.displayErrorMessage("")
      }
    }
    
    passwordTextField.onDidEndEditing = { [weak self] _ in
      
      let password = self?.passwordTextField.text ?? ""
      if !password.isValidPassword() {
        self?.passwordTextField.displayErrorMessage("Password should have at least 8 characters")
      } else {
        self?.passwordTextField.displayErrorMessage("")
      }
    }
    
    confirmPasswordTextField.onDidEndEditing = { [weak self] _ in
      
      let confirmPassword = self?.confirmPasswordTextField.text ?? ""
      if confirmPassword != (self?.passwordTextField.text ?? "") {
        self?.confirmPasswordTextField.displayErrorMessage("Password mismatch")
      } else {
        self?.passwordTextField.displayErrorMessage("")
      }
    }
  }
  
  private func setTextFieldShouldReturn() {
    
    fullNameTextField.onShouldReturn = { [weak self] _ in
      
      self?.passwordTextField.becomeFirstResponder() ?? true
    }
    
    passwordTextField.onShouldReturn = { [weak self] _ in
      
      self?.confirmPasswordTextField.becomeFirstResponder() ?? true
    }
    
    confirmPasswordTextField.onShouldReturn = { [weak self] _ in
      
      self?.view.endEditing(true)
      self?.updateUser()
      return true
    }
  }
}
