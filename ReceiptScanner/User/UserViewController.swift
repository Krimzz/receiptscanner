//
//  UserViewController.swift
//  ReceiptScanner
//
//  Created by Andrei Budisan on 26/04/2020.
//  Copyright © 2020 Andrei Budisan. All rights reserved.
//

import UIKit
import ViewModel
import RxSwift
import RxRelay
import NotificationBannerSwift

class UserViewController: UIViewController {
  
  @IBOutlet private weak var fullNameLabel: UILabel!
  @IBOutlet private weak var emailLabel: UILabel!
  
  private let authenticationViewModel = ViewModelFactory.makeAuthenticationViewModel()
  private let userViewModel = ViewModelFactory.makeUserViewModel()
  private lazy var loadingOverlay = LoadingOverlay()
  private let disposeBag = DisposeBag()
  
  override func viewDidLoad() {
    
    super.viewDidLoad()
    bindAuthentication()
  }
}

// MARK: - Button Action

extension UserViewController {
  
  @IBAction private func onLogoutPressed(_ sender: UIBarButtonItem) {
  
    authenticationViewModel.logOut()
  }
  
  @IBAction private func onEditUserPressed(_ sender: UIBarButtonItem) {
    
    let editUserViewController = EditUserViewController.instantiate()
    editUserViewController.fullName = fullNameLabel.text
    editUserViewController.editDelegate = self
    navigationController?.pushViewController(editUserViewController, animated: true)
  }
}

// MARK: - Binding

extension UserViewController {
  
  private func bindAuthentication() {
    
    authenticationViewModel
      .onLoginStatusChanged
      .observeOn(MainScheduler.asyncInstance)
      .subscribe(onNext: { [weak self] isLoggedIn in
        
        if isLoggedIn {
          self?.fetchUserData()
        }
      }).disposed(by: disposeBag)
  }
}

// MARK: - Behavior

extension UserViewController {
  
  private func fetchUserData() {
    
    if let uid = authenticationViewModel.currentUserId {
      
      loadingOverlay.show(onTopOf: view)
      userViewModel.user(uid: uid) { result in
        
        self.loadingOverlay.hide()
        switch result {
        case .success(let user):
          self.fullNameLabel.text = "\(user.firstName) \(user.lastName)"
          self.emailLabel.text = user.email
          
        case .failure(let error):
          GrowingNotificationBanner.userFetchFailed(withError: error.userFriendlyDescription).show()
        }
      }
    } else {
      GrowingNotificationBanner.userFetchFailed(withError: "Could not find user id.").show()
    }
  }
}

// MARK: - Edit User Delegate

extension UserViewController: EditUserDelegate {
  
  func didUpdateUser(with fullName: String) {
    
    DispatchQueue.main.async {
      
      if !fullName.isEmpty { self.fullNameLabel.text = fullName }
    }
  }
}
