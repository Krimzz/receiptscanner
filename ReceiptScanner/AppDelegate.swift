//
//  AppDelegate.swift
//  ReceiptScanner
//
//  Created by Andrei Budisan on 24/02/2020.
//  Copyright © 2020 Andrei Budisan. All rights reserved.
//

import UIKit

class AppDelegate: UIResponder, UIApplicationDelegate {
  
  var window: UIWindow?
  lazy var snapshotPreventionView: UIView = makeSnapshotPreventionView()

  func application(_ application: UIApplication,
                   didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

    if let appName = Bundle.main.object(forInfoDictionaryKey: "CFBundleName") as? String,
      let appVersion = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String {
      
      log.info("App: \(appName), Version: \(appVersion)")
    }
    
    return true
  }
}

// MARK: - Snapshot prevention

extension AppDelegate {
  
  func applicationDidBecomeActive(_ application: UIApplication) {
    
    snapshotPreventionView.removeFromSuperview()
  }
  
  func applicationWillResignActive(_ application: UIApplication) {
    
    guard let window = window else { return }
    snapshotPreventionView.frame = window.frame
    window.addSubview(snapshotPreventionView)
  }
  
  private func makeSnapshotPreventionView() -> UIView {
    
    guard let window = window else { return UIView() }
    
    let view = UIView(frame: window.frame)
    view.backgroundColor = .white
    
    let backgroundThumbImage = UIImageView()
    backgroundThumbImage.contentMode = .scaleAspectFit
    backgroundThumbImage.image = UIImage(named: "appLogo")
    backgroundThumbImage.backgroundColor = .white
    backgroundThumbImage.translatesAutoresizingMaskIntoConstraints = false
    backgroundThumbImage.clipsToBounds = true
    
    view.addSubview(backgroundThumbImage)
    
    backgroundThumbImage.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
    backgroundThumbImage.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
    backgroundThumbImage.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16).isActive = true
    
    return view
  }
}
