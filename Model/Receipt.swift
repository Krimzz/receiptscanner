//
//  Receipt.swift
//  Model
//
//  Created by Andrei Budisan on 26/04/2020.
//  Copyright © 2020 Andrei Budisan. All rights reserved.
//

import Foundation
import CoreLocation

public class Receipt: Codable {
  
  public var id: Identifier?
  public var owner: Identifier
  public var value: Double
  public var paymentTerm: Date
  public let location: CLLocationCoordinate2D
  
  private let locationX: Double
  private let locationY: Double
  
  enum CodingKeys: String, CodingKey {
    case id
    case owner
    case value
    case paymentTerm
    case locationX
    case locationY
  }
  
  required public init(from decoder: Decoder) throws {
    
    let container = try decoder.container(keyedBy: CodingKeys.self)
    
    id = try container.decodeIfPresent(Identifier.self, forKey: .id)
    owner = try container.decode(Identifier.self, forKey: .owner)
    value = try container.decode(Double.self, forKey: .value)
    paymentTerm = try container.decode(Date.self, forKey: .paymentTerm)
    
    locationX = try container.decode(Double.self, forKey: .locationX)
    locationY = try container.decode(Double.self, forKey: .locationY)
    location = CLLocationCoordinate2D(latitude: locationY, longitude: locationX)
  }
  
  public init(id: Identifier?,
              owner: Identifier,
              value: Double,
              paymentTerm: Date,
              location: CLLocationCoordinate2D) {
    
    self.id = id
    self.owner = owner
    self.value = value
    self.paymentTerm = paymentTerm
    
    self.location = location
    self.locationX = location.longitude
    self.locationY = location.latitude
  }
}

extension Receipt: CustomStringConvertible {
  
  public var description: String {
    "[Receipt: \(id ?? "noId"), \(value)]"
  }
}
