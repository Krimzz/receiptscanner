//
//  User.swift
//  Model
//
//  Created by Andrei Budisan on 26/04/2020.
//  Copyright © 2020 Andrei Budisan. All rights reserved.
//

import Foundation

public typealias Identifier = String

public class User: Codable {
  
  public var email: String
  public var firstName: String
  public var lastName: String
  public var uid: Identifier
  
  enum CodingKeys: String, CodingKey {
    case email
    case firstName
    case lastName
    case uid
  }
  
  required public init(from decoder: Decoder) throws {
    
    let container = try decoder.container(keyedBy: CodingKeys.self)
    
    email = try container.decode(String.self, forKey: .email)
    firstName = try container.decode(String.self, forKey: .firstName)
    lastName = try container.decode(String.self, forKey: .lastName)
    uid = try container.decode(Identifier.self, forKey: .uid)
  }
  
  public init(email: String,
              firstName: String,
              lastName: String,
              uid: Identifier) {
    
    self.email = email
    self.firstName = firstName
    self.lastName = lastName
    self.uid = uid
  }
}

extension User: CustomStringConvertible {
  
  public var description: String {
    "[User: \(email),\(uid)]"
  }
}
