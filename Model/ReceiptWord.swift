//
//  ImageText.swift
//  Model
//
//  Created by Andrei Budisan on 05/06/2020.
//  Copyright © 2020 Andrei Budisan. All rights reserved.
//

import Foundation
import CoreGraphics

public struct ReceiptWord {
  
  public let description: String
  public let vertices: [CGPoint]
  public let frame: CGRect
  
  public init(description: String, vertices: [CGPoint]) {
    
      self.description = description
      self.vertices = vertices
    
      let width = vertices[1].x - vertices[0].x
      let height = vertices[2].y - vertices[1].y
      let size = CGSize(width: width, height: height)
    
      self.frame = CGRect(origin: vertices[0], size: size)
  }
}
