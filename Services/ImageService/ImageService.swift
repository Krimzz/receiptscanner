//
//  ImageService.swift
//  Services
//
//  Created by Andrei Budisan on 10/05/2020.
//  Copyright © 2020 Andrei Budisan. All rights reserved.
//

import Foundation
import CoreGraphics
import Model

public protocol ImageService {
  
  func processImageBase64(_ imageBase64: String,
                          with size: CGSize,
                          completion: @escaping (Result<(ReceiptWord?, Date), ServiceError>) -> Void)
}
