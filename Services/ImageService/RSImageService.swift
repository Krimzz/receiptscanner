//
//  RSImageService.swift
//  Services
//
//  Created by Andrei Budisan on 10/05/2020.
//  Copyright © 2020 Andrei Budisan. All rights reserved.
//

import Foundation
import Networking
import CoreGraphics
import Model

public class RSImageService: ImageService {
  
  private var networkService: NetworkService
  private let requestBuilder = ImageRequestBuilder()
  
  init(networkService: NetworkService) {
    
    self.networkService = networkService
  }
  
  public func processImageBase64(_ imageBase64: String,
                                 with size: CGSize,
                                 completion: @escaping (Result<(ReceiptWord?, Date), ServiceError>) -> Void) {
    
    let request = requestBuilder.visionAPIRequest(imageBase64: imageBase64)
    
    networkService.send(request: request) { (result: Result<VisionResponse, NetworkingError>) in
      
      switch result {
      case .success(let visionResponse):
        
        guard let textAnnotations = visionResponse.responses.first?.textAnnotations else {
          log.error("No text returned from image")
          return completion(.failure(.imageProcessing))
        }
        
        var words: [ReceiptWord] = textAnnotations.map { annotation -> ReceiptWord in
          
          let vertices = annotation.boundingPoly.vertices.map { CGPoint(x: $0.x, y: $0.y) }
          return ReceiptWord(description: annotation.description, vertices: vertices)
        }
        words.removeFirst()
        
        let date = self.date(from: words)
        log.info("Receipt date found: \(String(describing: date))")
        
        var totalValue: ReceiptWord?
        
        if let totalString = self.textForTotal(from: words) {
          
          if let valueLeft = self.valueRightOfTotalText(totalString, from: words, imageSize: size) {
            log.info("Found value right of total: \(valueLeft)")
            totalValue = valueLeft
            
          } else {
            if let bellow = self.valueBelowTotalText(totalString, from: words, imageSize: size) {
              log.info("Found value bellow of total: \(bellow)")
              totalValue = bellow
              
            } else {
              if let valueWithoutTotal = self.valueForTotalFromAllReceipt(from: words) {
                log.info("Found value by biggest size of the word rectangle: \(valueWithoutTotal)")
                totalValue = valueWithoutTotal
              }
            }
          }
          
        } else {
          log.warning("Could not find word TOTAL in receipt. Trying to find value without it")
          
          if let valueWithoutTotal = self.valueForTotalFromAllReceipt(from: words) {
            log.info("Found value by biggest size of the word rectangle: \(valueWithoutTotal)")
            totalValue = valueWithoutTotal
          }
        }
        
        completion(.success((totalValue, date)))

      case .failure(let error):
        log.error("Failed to process image with error: \(error.localizedDescription)")
        completion(.failure(.imageProcessing))
      }
    }
  }
  
  private func textForTotal(from words: [ReceiptWord]) -> ReceiptWord? {
    
    let possibleTotalStrings = ["TOTAL", "TOTAL:", "Total", "Total:", "total", "total:",
                                "OTAL", "otal", "OTAL:", "otal:",
                                "TOTA", "Tota", "tota",
                                "OTA", "ota",
                                "SUMA", "SUMA:", "Suma", "Suma:", "suma", "suma:"]
    let totals = words.filter({ possibleTotalStrings.contains($0.description) })
    
    return totals.min(by: { $0.frame.maxY < $1.frame.maxY })
  }
  
  private func normalize(_ value: CGFloat) -> CGFloat {
    value < 0 ? 0 : value
  }
  
  private func valueRightOfTotalText(_ totalText: ReceiptWord,
                                     from words: [ReceiptWord],
                                     imageSize: CGSize) -> ReceiptWord? {
    
    var potentialTotalValues = [ReceiptWord]()
    
    var searchFrame = CGRect(x: totalText.frame.maxX,
                             y: normalize(totalText.frame.minY - totalText.frame.height),
                             width: normalize(imageSize.width - totalText.frame.maxX),
                             height: 5 * totalText.frame.height)
    
    let biggerFrameValues = words.filter({ searchFrame.contains($0.frame) })
    
    searchFrame = CGRect(x: totalText.frame.maxX,
                         y: normalize(totalText.frame.minY - (totalText.frame.height / 4)),
                         width: normalize(imageSize.width - totalText.frame.maxX),
                         height: 3 * totalText.frame.height)
    
    let inlineFrameValues = biggerFrameValues.filter({ searchFrame.contains($0.frame) })
    
    inlineFrameValues.forEach {
      if $0.description.isLike(regex: "\\d+[\\s,\\.\\-/]\\d{2}") {
        potentialTotalValues.append($0)
      }
    }
    
    var trueValues = [ReceiptWord]()
    
    potentialTotalValues.forEach({
        let description = $0.description.replacingOccurrences(of: "[,\\-/]",
                                                              with: ".", options: [.regularExpression],
                                                              range: nil)
        if let match = description.matches(forRegex: "\\d+[,\\.\\-/]\\d{2}").first {
            trueValues.append(ReceiptWord(description: match, vertices: $0.vertices))
        }
    })
    
    return trueValues.max(by: { $0.frame.height < $1.frame.height })
  }
  
  private func valueBelowTotalText(_ totalText: ReceiptWord,
                                   from words: [ReceiptWord],
                                   imageSize: CGSize) -> ReceiptWord? {
    
    var potentialTotalValues = [ReceiptWord]()
    
    var searchFrame = CGRect(x: normalize(totalText.frame.minX - (totalText.frame.width / 4)),
                             y: totalText.frame.maxY,
                             width: normalize(imageSize.width - totalText.frame.maxX),
                             height: 5 * totalText.frame.height)
    
    let biggerFrameValues = words.filter({ searchFrame.contains($0.frame) })
    
    searchFrame = CGRect(x: normalize(totalText.frame.minX - (totalText.frame.width / 4)),
                         y: totalText.frame.maxY,
                         width: normalize(imageSize.width - totalText.frame.maxX),
                         height: 2 * totalText.frame.height)
    
    let inlineFrameValues = biggerFrameValues.filter({ searchFrame.contains($0.frame) })
    
    inlineFrameValues.forEach {
      if $0.description.isLike(regex: "\\d+[\\s,\\.\\-/]\\d{2}") {
        potentialTotalValues.append($0)
      }
    }
    
    var trueValues = [ReceiptWord]()
    
    potentialTotalValues.forEach({
        let description = $0.description.replacingOccurrences(of: "[,\\-/]",
                                                              with: ".", options: [.regularExpression],
                                                              range: nil)
        if let match = description.matches(forRegex: "\\d+[,\\.\\-/]\\d{2}").first {
            trueValues.append(ReceiptWord(description: match, vertices: $0.vertices))
        }
    })
    
    return trueValues.max(by: { $0.frame.height < $1.frame.height })
  }
  
  private func valueForTotalFromAllReceipt(from words: [ReceiptWord]) -> ReceiptWord? {
    
    var possibleTotalTexts = [ReceiptWord]()

    words.filter { $0.description.isLike(regex: "\\d+[\\s,\\.\\-/]\\d{2}") }.forEach { (word) in
      
      let description = word.description.replacingOccurrences(of: "-", with: ",")
      
      if let match = description.matches(forRegex: "\\d+[,\\.\\-/]\\d{2}").first {
        possibleTotalTexts.append(ReceiptWord(description: match, vertices: word.vertices))
      }
    }
    
    return possibleTotalTexts.max(by: { $0.frame.height < $1.frame.height })
  }
  
  private func date(from words: [ReceiptWord]) -> Date {
    
    let dateRegex = "(\\d{2}[,\\.\\-/]){2}\\d{4}"
    let dateText = words.first(where: { $0.description.isLike(regex: dateRegex) })
    let timeRegex = "\\D*\\d{2}([\\-:]\\d{2}){1,2}"
    let timeText = words.first(where: { $0.description.isLike(regex: timeRegex) })
    
    guard let dateString = dateText?.description else {
      log.warning("No date was found in receipt")
      return Date()
    }
    
    if let timeString = timeText?.description {
      return Date().dateFrom(dateString: dateString, timeString: timeString)
    }
    
    return Date().dateFrom(dateString: dateString, timeString: "00:00")
  }
}
