//
//  VisionPayload.swift
//  Services
//
//  Created by Andrei Budisan on 10/05/2020.
//  Copyright © 2020 Andrei Budisan. All rights reserved.
//

import Foundation

struct VisionPayload: Encodable {
  
  let requests: [Request]
  
  struct Request: Codable {
    
    let image: Image
    let features: [Feature]
    
    struct Feature: Codable {
      let type: String
    }
    
    struct Image: Codable {
      let content: String
    }
  }
  
  init(imageBase64: String) {
    
    let feature = Request.Feature(type: "TEXT_DETECTION")
    let image = Request.Image(content: imageBase64)
    
    self.requests = [Request(image: image, features: [feature])]
  }
}
