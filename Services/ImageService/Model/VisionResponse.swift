//
//  VisionResponse.swift
//  Services
//
//  Created by Andrei Budisan on 05/06/2020.
//  Copyright © 2020 Andrei Budisan. All rights reserved.
//

import Foundation

// MARK: - VisionResponse
struct VisionResponse: Codable {
    let responses: [Response]
}

// MARK: - Response
struct Response: Codable {
    let textAnnotations: [TextAnnotation]
    let fullTextAnnotation: FullTextAnnotation
}

// MARK: - FullTextAnnotation
struct FullTextAnnotation: Codable {
    let pages: [Page]
    let text: String
}

// MARK: - Page
struct Page: Codable {
    let property: ParagraphProperty
    let width, height: Int
    let blocks: [Block]
}

// MARK: - Block
struct Block: Codable {
    let property: ParagraphProperty?
    let boundingBox: Bounding
    let paragraphs: [Paragraph]
    let blockType: String
}

// MARK: - Bounding
struct Bounding: Codable {
    let vertices: [Vertex]
}

// MARK: - Vertex
struct Vertex: Codable {
    let x, y: Int
}

// MARK: - Paragraph
struct Paragraph: Codable {
    let property: ParagraphProperty?
    let boundingBox: Bounding
    let words: [Word]
}

// MARK: - ParagraphProperty
struct ParagraphProperty: Codable {
    let detectedLanguages: [PurpleDetectedLanguage]
}

// MARK: - PurpleDetectedLanguage
struct PurpleDetectedLanguage: Codable {
    let languageCode: String
    let confidence: Double
}

// MARK: - Word
struct Word: Codable {
    let property: WordProperty?
    let boundingBox: Bounding
    let symbols: [Symbol]
}

// MARK: - WordProperty
struct WordProperty: Codable {
    let detectedLanguages: [FluffyDetectedLanguage]
}

// MARK: - FluffyDetectedLanguage
struct FluffyDetectedLanguage: Codable {
    let languageCode: String
}

// MARK: - Symbol
struct Symbol: Codable {
    let property: SymbolProperty?
    let boundingBox: Bounding
    let text: String
}

// MARK: - SymbolProperty
struct SymbolProperty: Codable {
    let detectedLanguages: [FluffyDetectedLanguage]?
    let detectedBreak: DetectedBreak?
}

// MARK: - DetectedBreak
struct DetectedBreak: Codable {
    let type: String
}

// MARK: - TextAnnotation
struct TextAnnotation: Codable {
    let locale: String?
    let description: String
    let boundingPoly: Bounding
}
