//
//  ImageRequestBuilder.swift
//  Services
//
//  Created by Andrei Budisan on 10/05/2020.
//  Copyright © 2020 Andrei Budisan. All rights reserved.
//

import Foundation

class ImageRequestBuilder: UrlBuilder {
  
  func visionAPIRequest(imageBase64: String) -> URLRequest {
    
    let url = visionUrl.appendingPathComponent("images:annotate")
    
    var request = URLRequest(baseURL: url, withQueryItems: [URLQueryItem(name: "key", value: apiKey)]).forJSONContent
    request.httpMethod = "POST"
    
    let payload = VisionPayload(imageBase64: imageBase64)
    request.httpBody = try? JSONEncoder().encode(payload)
    
    return request
  }
}
