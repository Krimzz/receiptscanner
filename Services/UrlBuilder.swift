//
//  UrlBuilder.swift
//  Services
//
//  Created by Andrei Budisan on 28/04/2020.
//  Copyright © 2020 Andrei Budisan. All rights reserved.
//

import Foundation

class UrlBuilder {
  
  private let authenticationString = "https://identitytoolkit.googleapis.com/v1/"
  private let firebaseString = "https://receiptscanner-utcn.firebaseio.com/"
  private let visionString = "https://vision.googleapis.com/v1/"
  
  let apiKey = "AIzaSyCxRoWt8cStqUc4xGUS8r9_X985rSvqIW4"
  
  var firebaseUrl: URL! {
    URL(string: firebaseString)
  }
  
  var authenticationUrl: URL! {
    URL(string: authenticationString)
  }
  
  var visionUrl: URL! {
    URL(string: visionString)
  }
}
