//
//  URLRequest+Convenience.swift
//  Services
//
//  Created by Andrei Budisan on 26/04/2020.
//  Copyright © 2020 Andrei Budisan. All rights reserved.
//

import Foundation

extension URLRequest {
  
  init(baseURL: URL, withQueryItems items: [URLQueryItem]) {
    
    var components: URLComponents! = URLComponents(url: baseURL, resolvingAgainstBaseURL: false)
    if !items.isEmpty {
      components.queryItems = items
    }
    
    guard let url = components.url else {
      fatalError("Could not create URL from path: '\(baseURL)' by adding query params: \(items)")
    }
    
    self = URLRequest(url: url)
  }
}
