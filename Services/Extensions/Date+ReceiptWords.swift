//
//  Date+ReceiptWords.swift
//  Services
//
//  Created by Andrei Budisan on 06/06/2020.
//  Copyright © 2020 Andrei Budisan. All rights reserved.
//

import Foundation

extension Date {
  
  private func correct(year: Int) -> Int {
    
    var year = year
    
    if year > 2800 {
      year -= 800
    }
    
    return year
  }
  
  private func correct(month: Int) -> Int {
    
    var month = month
    
    if !(1...12 ~= month) {
      if month == 18 {
        month = 10
      } else {
        month %= 10
      }
    }
    
    if month == 0 {
      month = 8
    }
    
    return month
  }
  
  private func correct(day: Int) -> Int {
    
    var day = day
    
    if !(1...31 ~= day) {
      if day == 38 {
        day = 30
      } else {
        day %= 10
      }
    }
    
    if day == 0 {
      day = 8
    }
    
    return day
  }
  
  private func correct(hour: Int) -> Int {
    
    var hour = hour
    
    if !(0...23 ~= hour) {
      if hour == 28 {
        hour = 20
      } else {
        hour %= 10
      }
    }
    
    return hour
  }
  
  private func correct(minutes: Int) -> Int {
    
    var minutes = minutes
    
    if !(0...59 ~= minutes) {
      minutes %= 10
    }
    
    return minutes
  }
  
  private func dateComponents(fromString source: String) -> DateComponents? {
    
    var foundDate = DateComponents()
    let components = source.components(separatedBy: CharacterSet(charactersIn: ",.-/"))
    
    guard var day = Int(components[0]), var month = Int(components[1]), var year = Int(components[2]) else {
      return nil
    }
    
    year = correct(year: year)
    month = correct(month: month)
    day = correct(day: day)
    
    log.info("Found date components -> Day: \(day), Month: \(month), Year: \(year)")
    
    foundDate.year = year
    foundDate.month = month
    foundDate.day = day
    
    return foundDate
  }
  
  private func timeComponents(fromString source: String) -> DateComponents {
    
    var foundTime = DateComponents()
    let components = source.split(whereSeparator: { $0 == ":" || $0 == "-" })
    
    guard var hour = Int(components[0]), var minutes = Int(components[1]) else {
      
      foundTime.hour = 0
      foundTime.minute = 0
      
      return foundTime
    }
    
    hour = correct(hour: hour)
    minutes = correct(minutes: minutes)
    
    log.info("Found time components -> Hour: \(hour), Minutes: \(minutes)")
    
    foundTime.hour = hour
    foundTime.minute = minutes
    
    return foundTime
  }
  
  func dateFrom(dateString: String, timeString: String) -> Date {
    
    log.info("Trying to get date and time from -> (Date: \(dateString), Time: \(timeString)")
    
    var components = DateComponents()
    let currentDate = Date()
    
    let dateRegex = "(\\d{2}[,\\.\\-/]){2}\\d{4}"
    let timeRegex = "(\\d{2}([\\-:]\\d{2}){1,2})"
    
    guard let dateMatch = dateString.matches(forRegex: dateRegex).first,
      let dateComponents = dateComponents(fromString: dateMatch) else {
        
        log.error("Failed to get date and time from strings")
        return currentDate
    }
    
    components.day = dateComponents.day
    components.month = dateComponents.month
    components.year = dateComponents.year
    components.hour = 0
    components.minute = 0
    
    if let timeMatch = timeString.matches(forRegex: timeRegex).first {
      let timeComponents = self.timeComponents(fromString: timeMatch)
      components.hour = timeComponents.hour
      components.minute = timeComponents.minute
    }
    
    if let dateFromComponents = Calendar.current.date(from: components), dateFromComponents <= currentDate {
      return dateFromComponents
    } else {
      return currentDate
    }
  }
}
