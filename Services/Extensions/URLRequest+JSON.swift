//
//  URLRequest+JSON.swift
//  Services
//
//  Created by Andrei Budisan on 26/04/2020.
//  Copyright © 2020 Andrei Budisan. All rights reserved.
//

import Foundation

extension URLRequest {

  var forJSONContent: URLRequest {

    guard let url = url else { return self }
    
    var jsonRequest = URLRequest(url: url)
    jsonRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
    return jsonRequest
  }
}
