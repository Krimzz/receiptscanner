//
//  URLRequest+URLEncoded.swift
//  Services
//
//  Created by Andrei Budisan on 26/04/2020.
//  Copyright © 2020 Andrei Budisan. All rights reserved.
//

import Foundation

extension URLRequest {

  var forURLEncodedContent: URLRequest {

    guard let url = url else { return self }
    
    var urlEncodedRequest = URLRequest(url: url)
    urlEncodedRequest.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
    return urlEncodedRequest
  }
}
