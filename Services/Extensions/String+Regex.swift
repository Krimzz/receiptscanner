//
//  String+Regex.swift
//  Services
//
//  Created by Andrei Budisan on 06/06/2020.
//  Copyright © 2020 Andrei Budisan. All rights reserved.
//

import Foundation

extension String {
  
  func isLike(regex stringRegex: String) -> Bool {
    
    do {
      let regex = try NSRegularExpression(pattern: stringRegex, options: .caseInsensitive)
      return regex.matches(in: self, options: [], range: NSRange(location: 0, length: self.count)).count == 1
      
    } catch {
      return false
    }
  }
  
  func matches(forRegex regex: String) -> [String] {
    
    do {
      let regex = try NSRegularExpression(pattern: regex)
      let results = regex.matches(in: self, options: [], range: NSRange(self.startIndex..., in: self))
      
      return results.compactMap {
        if let range = Range($0.range, in: self) {
          return String(self[range])
        } else {
          return nil
        }
      }
    } catch {
      log.error("Failed to use regex for string matchup with error: \(error.localizedDescription)")
      return []
    }
  }
}
