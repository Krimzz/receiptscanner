//
//  RSUserService.swift
//  Services
//
//  Created by Andrei Budisan on 28/04/2020.
//  Copyright © 2020 Andrei Budisan. All rights reserved.
//

import Foundation
import Model
import Networking

public class RSUserService: UserService {
  
  private let networkService: NetworkService
  private let requestBuilder = UserRequestBuilder()
  
  init(networkService: NetworkService) {
    self.networkService = networkService
  }
  
  public func create(uid: Identifier,
                     email: String,
                     fullName: String,
                     completion: @escaping (ServiceError?) -> Void) {
    
    let request = requestBuilder.createRequest(uid: uid, email: email, fullName: fullName)
    
    networkService.send(request: request) { (result: Result<User, NetworkingError>) in
      
      switch result {
      case .success:
        log.debug("Successfully created user with email \(email)")
        completion(nil)
      case .failure(let error):
        log.error("Failed to create user with error: \(error.localizedDescription)")
        completion(.createUser)
      }
    }
  }
  
  public func update(user: User,
                     completion: @escaping (ServiceError?) -> Void) {
    
    let request = requestBuilder.updateRequest(user: user)
    
    networkService.send(request: request) { (result: Result<User, NetworkingError>) in
      
      switch result {
      case .success:
        log.debug("Updating user: \(user) was successfull")
        completion(nil)
      case .failure(let error):
        log.error("Updating user failed with error: \(error.localizedDescription)")
        completion(.updateUser)
      }
    }
  }
  
  public func user(uid: Identifier,
                   completion: @escaping (Result<User, ServiceError>) -> Void) {
    
    let request = requestBuilder.getRequest(uid: uid)
    
    networkService.send(request: request) { (result: Result<User, NetworkingError>) in
      
      switch result {
      case .success(let user):
        log.debug("Successfully fetched user \(user)")
        completion(.success(user))
      case .failure(let error):
        log.error("Failed fetching user: \(uid) with error \(error.localizedDescription)")
        completion(.failure(.fetchUser))
      }
    }
  }
}
