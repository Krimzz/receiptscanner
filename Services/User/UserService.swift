//
//  UserService.swift
//  Services
//
//  Created by Andrei Budisan on 28/04/2020.
//  Copyright © 2020 Andrei Budisan. All rights reserved.
//

import Foundation
import Model

public protocol UserService {
  
  func create(uid: Identifier,
              email: String,
              fullName: String,
              completion: @escaping (ServiceError?) -> Void)
  func update(user: User, completion: @escaping (ServiceError?) -> Void)
  func user(uid: Identifier, completion: @escaping (Result<User, ServiceError>) -> Void)
}
