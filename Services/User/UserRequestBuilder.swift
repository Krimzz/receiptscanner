//
//  UserRequestBuilder.swift
//  Services
//
//  Created by Andrei Budisan on 28/04/2020.
//  Copyright © 2020 Andrei Budisan. All rights reserved.
//

import Foundation
import class Model.User
import typealias Model.Identifier

class UserRequestBuilder: UrlBuilder {

  private func userUrl(uid: Identifier) -> URL {

    firebaseUrl.appendingPathComponent("users/\(uid).json")
  }
  
  func getRequest(uid: Identifier) -> URLRequest {
    
    let url = userUrl(uid: uid)
    return URLRequest(url: url).forJSONContent
  }
  
  func updateRequest(user: User) -> URLRequest {
    
    let url = userUrl(uid: user.uid)
    var request = URLRequest(url: url).forJSONContent
    request.httpMethod = "PATCH"
    
    request.httpBody = try? JSONEncoder().encode(user)
    
    return request
  }
  
  func createRequest(uid: Identifier, email: String, fullName: String) -> URLRequest {
    
    let url = userUrl(uid: uid)
    var request = URLRequest(url: url).forJSONContent
    request.httpMethod = "PATCH"
    
    let firstName  = String(fullName.split(separator: " ")[0])
    let lastName = String(fullName.split(separator: " ")[1])
    
    let payload = User(email: email,
                       firstName: firstName,
                       lastName: lastName,
                       uid: uid)
    request.httpBody = try? JSONEncoder().encode(payload)
    
    return request
  }
}
