//
//  ServiceError.swift
//  Services
//
//  Created by Andrei Budisan on 26/04/2020.
//  Copyright © 2020 Andrei Budisan. All rights reserved.
//

import Foundation

public enum ServiceError: Error {

  // MARK: - Authorization Errors
  case login(error: String)
  case register(error: String)
  case updatePassword
  
  // MARK: - User Errors
  case createUser
  case updateUser
  case fetchUser
  
  // MARK: - Receipt Errors
  case addReceipt
  case updateReceipt
  case deleteReceipt
  case getReceipt
  case getAllReceipts
  case imageProcessing
  
  case unknown
}

extension ServiceError {
  
  public var userFriendlyDescription: String {
    switch self {
    case .login(let error):
      return error
    case .register(let error):
      return error
    case .updatePassword:
      return "A problem occured while trying to update your password."
    case .createUser:
      return "A problem occured while trying to create your account. Please relogin."
    case .updateUser:
      return "A problem occured while trying to update your account information."
    case .fetchUser:
      return "A problem occured while trying to get your account information."
    case .addReceipt:
      return "A problem occured while trying to add a new receipt."
    case .updateReceipt:
      return "A problem occured while trying to update the receipt."
    case .deleteReceipt:
      return "A problem occured while trying to delete a receipt."
    case .getReceipt:
      return "A problem occured while trying to the receipt."
    case .getAllReceipts:
      return "A problem occured while trying to get all your receipts."
    case .imageProcessing:
      return "A problem occured trying to identify the information from the receipt"
    case .unknown:
      return "An unknown error has occured."
    }
  }
}
