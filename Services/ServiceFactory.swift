//
//  ServiceFactory.swift
//  Services
//
//  Created by Andrei Budisan on 23/04/2020.
//  Copyright © 2020 Andrei Budisan. All rights reserved.
//

import Foundation
import Networking

public class ServiceFactory {
  
  public var network: NetworkService
  public var authentication: AuthenticationService
  public var user: UserService
  public var receipts: ReceiptService
  public var image: ImageService
  
  public init() {
    network = AFNetworkService()
    let authentication = RSAuthenticationService(networkService: network)
    network.delegate = authentication
    self.authentication = authentication
    self.user = RSUserService(networkService: network)
    self.receipts = RSReceiptService(networkService: network)
    self.image = RSImageService(networkService: network)
  }
}
