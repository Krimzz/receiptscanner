//
//  AuthenticationRequestBuilder.swift
//  Services
//
//  Created by Andrei Budisan on 26/04/2020.
//  Copyright © 2020 Andrei Budisan. All rights reserved.
//

import Foundation

class AuthenticationRequestBuilder: UrlBuilder {
  
  private enum UrlType: String {
    case login = "signInWithPassword"
    case register = "signUp"
    case updatePassword = "update"
    case idToken
  }
  
  func loginRequest(email: String, password: String) -> URLRequest {
    
    let url = authenticationUrl.appendingPathComponent("accounts:signInWithPassword")

    var request = URLRequest(baseURL: url, withQueryItems: [URLQueryItem(name: "key", value: apiKey)]).forJSONContent
    request.httpMethod = "POST"
    
    let payload = AuthenticationPayload(email: email,
                                        password: password,
                                        displayName: nil,
                                        returnSecureToken: true)
    request.httpBody = try? JSONEncoder().encode(payload)
    
    return request
  }
  
  func registerRequest(fullName: String, email: String, password: String) -> URLRequest {
    
    let url = authenticationUrl.appendingPathComponent("accounts:signUp")

    var request = URLRequest(baseURL: url, withQueryItems: [URLQueryItem(name: "key", value: apiKey)]).forJSONContent
    request.httpMethod = "POST"
    
    let payload = AuthenticationPayload(email: email,
                                        password: password,
                                        displayName: fullName,
                                        returnSecureToken: true)
    request.httpBody = try? JSONEncoder().encode(payload)
    
    return request
  }
  
  func updatePasswordRequest(newPassword: String, idToken: String) -> URLRequest {
    
    let url = authenticationUrl.appendingPathComponent("accounts:update")

    var request = URLRequest(baseURL: url, withQueryItems: [URLQueryItem(name: "key", value: apiKey)]).forJSONContent
    request.httpMethod = "POST"
    
    let payload = UpdatePasswordPayload(idToken: idToken,
                                        password: newPassword,
                                        returnSecureToken: true)
    request.httpBody = try? JSONEncoder().encode(payload)
    
    return request
  }
  
  func idTokenRequest(refreshToken: String) -> URLRequest {
    
    let url = authenticationUrl.appendingPathComponent("token")

    var request =
      URLRequest(baseURL: url, withQueryItems: [URLQueryItem(name: "key", value: apiKey)]).forURLEncodedContent
    request.httpMethod = "POST"
    
    var payload = URLComponents()
    payload.queryItems = [URLQueryItem(name: "grant_type", value: "refresh_token"),
                          URLQueryItem(name: "refreshToken", value: refreshToken)]
    request.httpBody = payload.query?.data(using: .utf8)
    
    return request
  }
  
  func shouldAuthenticate(urlString: String) -> Bool {
    
    urlString.contains(firebaseUrl.absoluteString)
  }
}
