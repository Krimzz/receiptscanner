//
//  AuthenticationService.swift
//  Services
//
//  Created by Andrei Budisan on 26/04/2020.
//  Copyright © 2020 Andrei Budisan. All rights reserved.
//

import Foundation
import Networking
import RxSwift
import RxRelay
import typealias Model.Identifier

public protocol AuthenticationService {
  
  var onLoginStatusChanged: BehaviorRelay<Bool> { get }
  var reachability: BehaviorRelay<Bool> { get }
  var currentUserId: Identifier? { get }
  
  func logOut()
  func logIn(email: String, password: String, completion: @escaping (ServiceError?) -> Void)
  func register(fullName: String,
                email: String,
                password: String,
                completion: @escaping (Result<String, ServiceError>) -> Void)
  func updatePassword(to password: String, completion: @escaping (ServiceError?) -> Void)
}
