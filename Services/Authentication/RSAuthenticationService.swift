//
//  RSAuthenticationService.swift
//  Services
//
//  Created by Andrei Budisan on 26/04/2020.
//  Copyright © 2020 Andrei Budisan. All rights reserved.
//

import Foundation
import Networking
import KeychainSwift
import RxRelay
import RxSwift
import typealias Model.Identifier

public class RSAuthenticationService: AuthenticationService {
  
  private enum KeychainLocations: String {
    case refreshToken
    case idToken
    case userId
  }
  
  private var networkService: NetworkService
  private var requestBuilder: AuthenticationRequestBuilder
  private var keychain: KeychainSwift
  private var decoder: JSONDecoder
  
  public let onLoginStatusChanged: BehaviorRelay<Bool>
  public let reachability = BehaviorRelay<Bool>(value: true)
  
  public var currentUserId: Identifier? {
    keychain.get(KeychainLocations.userId.rawValue)
  }
  
  init(networkService: NetworkService) {
    
    self.networkService = networkService
    requestBuilder = AuthenticationRequestBuilder()
    keychain = KeychainSwift()
    decoder = JSONDecoder()
    
    onLoginStatusChanged = BehaviorRelay<Bool>(value: keychain.get(KeychainLocations.idToken.rawValue) != nil)
    
    bindReachability()
  }
  
  public func logIn(email: String, password: String, completion: @escaping (ServiceError?) -> Void) {
    
    let request = requestBuilder.loginRequest(email: email, password: password)
    
    networkService.send(request: request) { (result: Result<LogInResponse, NetworkingError>) in
      
      switch result {
      case .success(let data):
        
        self.updateTokens(refreshToken: data.refreshToken, idToken: data.idToken, localId: data.localId)
        self.onLoginStatusChanged.accept(true)
        
        completion(nil)
        
      case .failure(let error):
        switch error {
        case .authenticationError(let authenticationError):
          log.error("Login failed with error: \(authenticationError.errorDescription ?? "")")
          completion(.login(error: authenticationError.userFriendlyDescription))
          
        case .decodingError(let decodingError):
          log.error("Could not decode login response with error: \(decodingError)")
          completion(.login(error: decodingError))
          
        default:
          completion(.unknown)
        }
      }
    }
  }
  
  public func register(fullName: String,
                       email: String,
                       password: String,
                       completion: @escaping (Result<String, ServiceError>) -> Void) {
    
    let request = requestBuilder.registerRequest(fullName: fullName, email: email, password: password)
    
    networkService.send(request: request) { (result: Result<RegisterResponse, NetworkingError>) in
      
      switch result {
      case .success(let data):
        
        self.updateTokens(refreshToken: data.refreshToken, idToken: data.idToken, localId: data.localId)
        self.onLoginStatusChanged.accept(true)
        
        completion(.success(data.localId))
        
      case .failure(let error):
        switch error {
        case .authenticationError(let authenticationError):
          log.error("Registration failed with error: \(authenticationError.errorDescription ?? "")")
          completion(.failure(.register(error: authenticationError.userFriendlyDescription)))
          
        case .decodingError(let decodingError):
          log.error("Could not decode registration response with error: \(decodingError)")
          completion(.failure(.register(error: decodingError)))
          
        default:
          completion(.failure(.unknown))
        }
      }
    }
  }
  
  public func logOut() {
    
    keychain.delete(KeychainLocations.refreshToken.rawValue)
    keychain.delete(KeychainLocations.idToken.rawValue)
    keychain.delete(KeychainLocations.userId.rawValue)
    
    onLoginStatusChanged.accept(false)
  }
  
  public func updatePassword(to password: String, completion: @escaping (ServiceError?) -> Void) {
    
    if let token = keychain.get(KeychainLocations.idToken.rawValue) {
      let request = requestBuilder.updatePasswordRequest(newPassword: password, idToken: token)
      
      networkService.send(request: request) { (result: Result<UpdatePasswordResponse, NetworkingError>) in
        
        switch result {
        case .success(let data):
          self.keychain.set(data.refreshToken, forKey: KeychainLocations.refreshToken.rawValue)
          self.keychain.set(data.idToken, forKey: KeychainLocations.idToken.rawValue)
          completion(nil)
          
        case .failure(let error):
          log.error("Failed to decode update password response with error: \(error)")
          completion(.updatePassword)
        }
      }
    } else {
      log.error("No refresh token found in keychain.")
      completion(.updatePassword)
    }
  }
  
  private func updateTokens(refreshToken: String, idToken: String, localId: String) {
    
    keychain.set(refreshToken, forKey: KeychainLocations.refreshToken.rawValue)
    keychain.set(idToken, forKey: KeychainLocations.idToken.rawValue)
    keychain.set(localId, forKey: KeychainLocations.userId.rawValue)
  }
}

extension RSAuthenticationService: AuthenticationDelegate {
  
  public func authorize(request: URLRequest, _ completion: @escaping (Result<URLRequest, Error>) -> Void) {
    
    guard let url = request.url else {
      log.error("Tried to authorize request: \(request) with no URL set.")
      completion(.failure(URLError(.badURL)))
      return
    }
    
    if shouldAdapt(url.absoluteString) {
      
      guard var urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: false) else {
        log.warning("Could not authorize url: \(url) because getting URLComponents failed.")
        completion(.failure(URLError(.badURL)))
        return
      }
      
      if let token = keychain.get(KeychainLocations.idToken.rawValue) {
        
        let authQueryItem = URLQueryItem(name: "auth", value: token)
        urlComponents.queryItems = (urlComponents.queryItems ?? []).filter { $0.name != "auth" } + [authQueryItem]
        
        var authorizedRequest = request
        authorizedRequest.url = urlComponents.url

        completion(.success(authorizedRequest))
      } else {
        log.error("Did not adapt url: \(url.absoluteString). Token missing from keychain.")
      }
    } else {
      log.debug("Did not adapt url: \(url.absoluteString).")
      completion(.success(request))
    }
  }
  
  public func requestNewIdToken(completion: @escaping (Bool) -> Void) {
    
    if let refreshToken = keychain.get(KeychainLocations.refreshToken.rawValue) {
      
      log.debug("Did start perform token refresh.")
      let request = requestBuilder.idTokenRequest(refreshToken: refreshToken)
      networkService.send(request: request) { (result: Result<TokenRefreshResponse, NetworkingError>) in
        
        switch result {
        case .success(let token):
          self.keychain.set(token.idToken, forKey: KeychainLocations.idToken.rawValue)
          completion(true)
        case .failure(let error):
          log.error("Failed refreshing token with error: \(error)")
          completion(false)
        }
      }
    } else {
      log.error("No refresh token found in keychain.")
      completion(false)
    }
  }
  
  private func shouldAdapt(_ urlString: String) -> Bool {
    requestBuilder.shouldAuthenticate(urlString: urlString)
  }
}

// MARK: - Binding

extension RSAuthenticationService {
  
  private func bindReachability() {
    
    networkService.reachability { [weak self] isReachable in
      
      if !isReachable {
        log.error("Network is unreachable")
      }
      
      self?.reachability.accept(isReachable)
    }
  }
}
