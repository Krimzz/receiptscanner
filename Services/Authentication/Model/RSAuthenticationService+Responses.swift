//
//  RSAuthenticationService+Responses.swift
//  Services
//
//  Created by Andrei Budisan on 26/04/2020.
//  Copyright © 2020 Andrei Budisan. All rights reserved.
//

import Foundation

extension RSAuthenticationService {
  
  struct LogInResponse: Decodable {
    
    public var kind: String
    public var localId: String
    public var email: String
    public var displayName: String
    public var idToken: String
    public var registered: Bool
    public var refreshToken: String
    public var expiresIn: String
  }

  struct RegisterResponse: Decodable {
    
    public var kind: String
    public var idToken: String
    public var displayName: String
    public var email: String
    public var refreshToken: String
    public var expiresIn: String
    public var localId: String
  }
  
  struct UpdatePasswordResponse: Decodable {
    
    public var kind: String
    public var localId: String
    public var email: String
    public var displayName: String
    public var idToken: String
    public var providerUserInfo: [[String: String]]
    public var refreshToken: String
    public var expiresIn: String
    public var passwordHash: String
    public var emailVerified: Bool
  }
  
  struct TokenRefreshResponse: Decodable {
    
    public var accessToken: String
    public var expiresIn: String
    public var tokenType: String
    public var refreshToken: String
    public var idToken: String
    public var userId: String
    public var projectId: String
    
    enum CodingKeys: String, CodingKey {
      case accessToken = "access_token"
      case expiresIn = "expires_in"
      case tokenType = "token_type"
      case refreshToken = "refresh_token"
      case idToken = "id_token"
      case userId = "user_id"
      case projectId = "project_id"
    }
  }
}
