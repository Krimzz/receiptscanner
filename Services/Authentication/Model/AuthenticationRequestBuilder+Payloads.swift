//
//  AuthenticationRequestBuilder+Payloads.swift
//  Services
//
//  Created by Andrei Budisan on 26/04/2020.
//  Copyright © 2020 Andrei Budisan. All rights reserved.
//

import Foundation

extension AuthenticationRequestBuilder {
  
  struct AuthenticationPayload: Encodable {
    
    let email: String
    let password: String
    let displayName: String?
    let returnSecureToken: Bool

    enum CodingKeys: String, CodingKey {

      case email
      case password
      case displayName
      case returnSecureToken
    }

    public func encode(to encoder: Encoder) throws {

      var container = encoder.container(keyedBy: CodingKeys.self)
      
      try container.encode(email, forKey: .email)
      try container.encode(password, forKey: .password)
      try container.encodeIfPresent(displayName, forKey: .displayName)
      try container.encode(returnSecureToken, forKey: .returnSecureToken)
    }
  }

  struct UpdatePasswordPayload: Encodable {
    
    let idToken: String
    let password: String
    let returnSecureToken: Bool

    enum CodingKeys: String, CodingKey {

      case idToken
      case password
      case returnSecureToken
    }

    public func encode(to encoder: Encoder) throws {

      var container = encoder.container(keyedBy: CodingKeys.self)
      
      try container.encode(idToken, forKey: .idToken)
      try container.encode(password, forKey: .password)
      try container.encode(returnSecureToken, forKey: .returnSecureToken)
    }
  }
}
