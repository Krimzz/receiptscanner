//
//  CreationResponse.swift
//  Services
//
//  Created by Andrei Budisan on 09/05/2020.
//  Copyright © 2020 Andrei Budisan. All rights reserved.
//

import Foundation

struct CreationResponse: Decodable {
  
  public var name: String
}
