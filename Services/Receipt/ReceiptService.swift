//
//  ReceiptService.swift
//  Services
//
//  Created by Andrei Budisan on 06/05/2020.
//  Copyright © 2020 Andrei Budisan. All rights reserved.
//

import Foundation
import Model

public protocol ReceiptService {
  
  func add(receipt: Receipt, completion: @escaping (ServiceError?) -> Void)
  func deleteReceipt(withId receiptId: Identifier, completion: @escaping (ServiceError?) -> Void)
  func update(receipt: Receipt, completion: @escaping (ServiceError?) -> Void)
  func receipt(withId receiptId: Identifier, completion: @escaping (Result<Receipt, ServiceError>) -> Void)
  func allReceipts(completion: @escaping (Result<[Receipt], ServiceError>) -> Void)
}
