//
//  ReceiptRequestBuilder.swift
//  Services
//
//  Created by Andrei Budisan on 06/05/2020.
//  Copyright © 2020 Andrei Budisan. All rights reserved.
//

import Foundation
import Model

class ReceiptRequestBuilder: UrlBuilder {
  
  public func addRequest(receipt: Receipt) -> URLRequest {
    
    let url = firebaseUrl.appendingPathComponent("receipts.json")
    var request = URLRequest(url: url).forJSONContent
    request.httpMethod = "POST"
    
    request.httpBody = try? JSONEncoder().encode(receipt)
    
    return request
  }
  
  public func deleteRequest(receiptId: Identifier) -> URLRequest {
    
    let url = firebaseUrl.appendingPathComponent("receipts/\(receiptId).json")
    var request = URLRequest(url: url)
    request.httpMethod = "DELETE"
    
    return request
  }
  
  public func updateRequest(receipt: Receipt) -> URLRequest {
    
    let url = firebaseUrl.appendingPathComponent("receipts/\(receipt.id ?? "").json")
    var request = URLRequest(url: url).forJSONContent
    request.httpMethod = "PATCH"
    
    request.httpBody = try? JSONEncoder().encode(receipt)
    
    return request
  }
  
  public func getRequest(receiptId: Identifier) -> URLRequest {
    
    let url = firebaseUrl.appendingPathComponent("receipts/\(receiptId).json")
    return URLRequest(url: url).forJSONContent
  }
  
  public var allReceiptsRequest: URLRequest {

    let url = firebaseUrl.appendingPathComponent("receipts.json")
    return URLRequest(url: url).forJSONContent
  }
}
