//
//  RSReceiptService.swift
//  Services
//
//  Created by Andrei Budisan on 06/05/2020.
//  Copyright © 2020 Andrei Budisan. All rights reserved.
//

import Foundation
import Model
import Networking

public class RSReceiptService: ReceiptService {
  
  private var networkService: NetworkService
  private let requestBuilder = ReceiptRequestBuilder()
  
  init(networkService: NetworkService) {
    
    self.networkService = networkService
  }
  
  public func add(receipt: Receipt, completion: @escaping (ServiceError?) -> Void) {
    
    let request = requestBuilder.addRequest(receipt: receipt)
    
    networkService.send(request: request) { (result: Result<CreationResponse, NetworkingError>) in
      
      switch result {
      case .success:
        log.debug("Receipt added with success: \(receipt)")
        completion(nil)
      case .failure(let error):
        log.error("Failed to add receipt: \(error.localizedDescription)")
        completion(.addReceipt)
      }
    }
  }
  
  public func deleteReceipt(withId receiptId: Identifier, completion: @escaping (ServiceError?) -> Void) {
    
    let request = requestBuilder.deleteRequest(receiptId: receiptId)
    
    networkService.send(request: request) { (result: Result<Receipt, NetworkingError>) in
      
      switch result {
      case .success:
        log.debug("Receipt deleted successfully: \(receiptId)")
        completion(nil)
      case .failure(let error):
        if error == .missingServerResponse {
          completion(nil)
        } else {
          log.error("Failed to delete receipt: \(receiptId) with error: \(error)")
          completion(.deleteReceipt)
        }
      }
    }
  }
  
  public func update(receipt: Receipt, completion: @escaping (ServiceError?) -> Void) {
    
    let request = requestBuilder.updateRequest(receipt: receipt)
    
    networkService.send(request: request) { (result: Result<Receipt, NetworkingError>) in
      
      switch result {
      case .success:
        log.debug("Receipt updated successfully: \(receipt)")
        completion(nil)
      case .failure(let error):
        log.error("Failed to update receipt: \(receipt) with error: \(error)")
        completion(.updateReceipt)
      }
    }
  }
  
  public func receipt(withId receiptId: Identifier, completion: @escaping (Result<Receipt, ServiceError>) -> Void) {
    
    let request = requestBuilder.getRequest(receiptId: receiptId)
    
    networkService.send(request: request) { (result: Result<Receipt, NetworkingError>) in
      
      switch result {
      case .success(let receipt):
        log.debug("Receipt fetched successfully: \(receipt)")
        completion(.success(receipt))
      case .failure(let error):
        log.error("Failed to fetch receipt: \(receiptId) with error: \(error)")
        completion(.failure(.getReceipt))
      }
    }
  }
  
  public func allReceipts(completion: @escaping (Result<[Receipt], ServiceError>) -> Void) {
    
    let request = requestBuilder.allReceiptsRequest
    
    networkService.send(request: request) { (result: Result<[String: Receipt], NetworkingError>) in
      
      switch result {
      case .success(let receiptsDictionary):
        log.debug("Receipts fetched successfully")
        
        var receipts = [Receipt]()
        receiptsDictionary.forEach { receipt in
          receipt.value.id = receipt.key
          receipts.append(receipt.value)
        }
        completion(.success(receipts))
        
      case .failure(let error):
        if error == .missingServerResponse {
          completion(.success([]))
        } else {
          log.error("Failed to fetch all receipts with error: \(error)")
          completion(.failure(.getAllReceipts))
        }
      }
    }
  }
}
