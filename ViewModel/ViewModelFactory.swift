//
//  ViewModelFactory.swift
//  ViewModel
//
//  Created by Andrei Budisan on 26/04/2020.
//  Copyright © 2020 Andrei Budisan. All rights reserved.
//

import Foundation
import Services

public class ViewModelFactory {
  
  public static var serviceFactory = ServiceFactory()

  public static func makeAuthenticationViewModel() -> AuthenticationViewModel {
    
    RSAuthenticationViewModel(authService: serviceFactory.authentication,
                              userService: serviceFactory.user)
  }
  
  public static func makeUserViewModel() -> UserViewModel {
    
    RSUserViewModel(userService: serviceFactory.user,
                    authService: serviceFactory.authentication)
  }
  
  public static func makeReceiptViewModel() -> ReceiptsViewModel {
    
    RSReceiptsViewModel(receiptService: serviceFactory.receipts,
                        userService: serviceFactory.user,
                        authenticationService: serviceFactory.authentication,
                        imageService: serviceFactory.image)
  }
}
