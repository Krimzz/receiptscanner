//
//  RSUserViewModel.swift
//  ViewModel
//
//  Created by Andrei Budisan on 29/04/2020.
//  Copyright © 2020 Andrei Budisan. All rights reserved.
//

import Foundation
import Services
import Model

public class RSUserViewModel: UserViewModel {
  
  private let userService: UserService
  private let authService: AuthenticationService
  private let dispatchGroup = DispatchGroup()

  init(userService: UserService, authService: AuthenticationService) {
    
    self.userService = userService
    self.authService = authService
  }
  
  public func user(uid: Identifier, completion: @escaping (Result<User, ServiceError>) -> Void) {
    
    userService.user(uid: uid, completion: completion)
  }
  
  public func update(uid: Identifier,
                     name: String,
                     password: String,
                     completion: @escaping (ErrorMessage?) -> Void) {
    
    userService.user(uid: uid, completion: { [weak self] result in
      
      switch result {
      case .success(let user):
        
        if !name.isEmpty {
          user.firstName = String(name.split(separator: " ")[0])
          user.lastName = String(name.split(separator: " ")[1])
        }
        var dispatchError: ServiceError?
        
        self?.dispatchGroup.enter()
        self?.userService.update(user: user, completion: { error in
         
          dispatchError = error
          self?.dispatchGroup.leave()
        })
        
        if !password.isEmpty {
          self?.dispatchGroup.enter()
          self?.authService.updatePassword(to: password, completion: { error in
            
            dispatchError = error
            self?.dispatchGroup.leave()
          })
        }
        
        self?.dispatchGroup.notify(queue: .main) {
          completion(dispatchError?.userFriendlyDescription)
        }
      case .failure(let error):
        completion(error.userFriendlyDescription)
      }
    })
  }
}
