//
//  UserViewModel.swift
//  ViewModel
//
//  Created by Andrei Budisan on 29/04/2020.
//  Copyright © 2020 Andrei Budisan. All rights reserved.
//

import Foundation
import Services
import Model

public protocol UserViewModel {
  
  func user(uid: Identifier, completion: @escaping (Result <User, ServiceError>) -> Void )
  func update(uid: Identifier,
              name: String,
              password: String,
              completion: @escaping (ErrorMessage?) -> Void)
}
