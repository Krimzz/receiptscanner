//
//  RSReceiptsViewModel.swift
//  ViewModel
//
//  Created by Andrei Budisan on 06/05/2020.
//  Copyright © 2020 Andrei Budisan. All rights reserved.
//

import Foundation
import RxRelay
import RxSwift
import Services
import Model

public class RSReceiptsViewModel: ReceiptsViewModel {
  
  public var displayedItems = BehaviorRelay<[Receipt]>(value: [])
  
  private let receiptService: ReceiptService
  private let userService: UserService
  private let authenticationService: AuthenticationService
  private let imageService: ImageService
  
  private var allItems = [Receipt]()
  private let disposeBag = DisposeBag()
  
  public init(receiptService: ReceiptService,
              userService: UserService,
              authenticationService: AuthenticationService,
              imageService: ImageService) {
    
    self.receiptService = receiptService
    self.userService = userService
    self.authenticationService = authenticationService
    self.imageService = imageService
    
    bindAuthenticationService()
  }
}

// MARK: - Public Behavior

extension RSReceiptsViewModel {
  
  public func refresh(completion: @escaping (ErrorMessage?) -> Void) {
    
    fetchReceipts(completion)
  }
  
  public func add(receipt: Receipt, completion: @escaping (ErrorMessage?) -> Void) {
    
    receiptService.add(receipt: receipt) { error in
      
      if let error = error {
        completion(error.userFriendlyDescription)
      } else {
        completion(nil)
      }
    }
  }
  
  public func deleteReceipt(withId receiptId: Identifier, completion: @escaping (ErrorMessage?) -> Void) {
    
    receiptService.deleteReceipt(withId: receiptId) { error in
      
      if let error = error {
        completion(error.userFriendlyDescription)
      } else {
        self.allItems.removeAll(where: { $0.id == receiptId })
        self.displayedItems.accept(self.allItems)
        completion(nil)
      }
    }
  }
  
  public func update(receipt: Receipt, completion: @escaping (ErrorMessage?) -> Void) {
    
    receiptService.update(receipt: receipt) { error in
      
      if let error = error {
        completion(error.userFriendlyDescription)
      } else {
        completion(nil)
      }
    }
  }
  
  public func receipt(withId receiptId: Identifier, completion: @escaping (Result<Receipt, ServiceError>) -> Void) {
    
    receiptService.receipt(withId: receiptId, completion: completion)
  }
  
  public func processReceiptImage(imageBase64: String,
                                  with size: CGSize,
                                  completion: @escaping (Result<(ReceiptWord?, Date), ServiceError>) -> Void) {
    
    imageService.processImageBase64(imageBase64, with: size, completion: completion)
  }
}

// MARK: - Private Behavior

extension RSReceiptsViewModel {
  
  private func fetchReceipts(_ completion: ((ErrorMessage?) -> Void)? = nil) {
    
    receiptService.allReceipts { result in
      
      switch result {
      case .success(let receipts):
        
        let userReceipts = receipts.filter { $0.owner == self.authenticationService.currentUserId }
        
        let sortedReceipts = userReceipts.sorted(by: {
          $0.paymentTerm.compare($1.paymentTerm) == .orderedAscending
        })
        
        self.allItems = sortedReceipts
        self.displayedItems.accept(self.allItems)
        completion?(nil)
        
      case .failure(let error):
        completion?(error.userFriendlyDescription)
      }
    }
  }
}

// MARK: - Binding

extension RSReceiptsViewModel {
  
  private func bindAuthenticationService() {
  
    authenticationService
      .onLoginStatusChanged
      .observeOn(MainScheduler.asyncInstance)
      .subscribe(onNext: { [weak self] isLoggedIn in
        
        if isLoggedIn {
          self?.fetchReceipts()
        } else {
          self?.allItems = []
          self?.displayedItems.accept([])
        }
      }).disposed(by: disposeBag)
  }
}
