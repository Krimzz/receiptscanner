//
//  ReceiptsViewModel.swift
//  ViewModel
//
//  Created by Andrei Budisan on 06/05/2020.
//  Copyright © 2020 Andrei Budisan. All rights reserved.
//

import Foundation
import Model
import RxSwift
import RxRelay
import Services

public protocol ReceiptsViewModel {
  
  var displayedItems: BehaviorRelay<[Receipt]> { get }
  
  func refresh(completion: @escaping (ErrorMessage?) -> Void)
  func add(receipt: Receipt, completion: @escaping (ErrorMessage?) -> Void)
  func deleteReceipt(withId receiptId: Identifier, completion: @escaping (ErrorMessage?) -> Void)
  func update(receipt: Receipt, completion: @escaping (ErrorMessage?) -> Void)
  func receipt(withId receiptId: Identifier, completion: @escaping (Result<Receipt, ServiceError>) -> Void)
  func processReceiptImage(imageBase64: String,
                           with size: CGSize,
                           completion: @escaping (Result<(ReceiptWord?, Date), ServiceError>) -> Void)
}
