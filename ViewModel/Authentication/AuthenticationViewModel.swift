//
//  AuthenticationViewModel.swift
//  ViewModel
//
//  Created by Andrei Budisan on 26/04/2020.
//  Copyright © 2020 Andrei Budisan. All rights reserved.
//

import Foundation
import RxSwift
import RxRelay
import typealias Model.Identifier

public typealias ErrorMessage = String

public protocol AuthenticationViewModel {

  var currentUserId: Identifier? { get }
  var onLoginStatusChanged: BehaviorRelay<Bool> { get }
  var reachability: BehaviorRelay<Bool> { get }
  
  func registerUser(fullName: String, email: String, password: String, completion: @escaping (ErrorMessage?) -> Void)
  func logInUser(email: String, password: String, completion: @escaping (ErrorMessage?) -> Void)
  func updatePassword(to password: String, completion: @escaping (ErrorMessage?) -> Void)
  func logOut()
}
