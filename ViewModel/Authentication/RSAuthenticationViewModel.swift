//
//  RSAuthenticationViewModel.swift
//  ViewModel
//
//  Created by Andrei Budisan on 26/04/2020.
//  Copyright © 2020 Andrei Budisan. All rights reserved.
//

import Foundation
import Services
import RxSwift
import RxRelay
import typealias Model.Identifier

public class RSAuthenticationViewModel: AuthenticationViewModel {
  
  private var authService: AuthenticationService
  private let userService: UserService
  
  init(authService: AuthenticationService,
       userService: UserService) {
    
    self.authService = authService
    self.userService = userService
  }
  
  public var currentUserId: Identifier? {
    authService.currentUserId
  }
  
  public var onLoginStatusChanged: BehaviorRelay<Bool> {
    authService.onLoginStatusChanged
  }
  
  public var reachability: BehaviorRelay<Bool> {
    authService.reachability
  }
  
  public func registerUser(fullName: String,
                           email: String,
                           password: String,
                           completion: @escaping (ErrorMessage?) -> Void) {
    
    authService.register(fullName: fullName, email: email, password: password) { result in
      
      switch result {
      case .success(let uid):
        self.userService.create(uid: uid, email: email, fullName: fullName) { error in
          
          if let error = error {
            completion(error.userFriendlyDescription)
          } else {
            completion(nil)
          }
        }
        
      case .failure(let error):
        completion(error.userFriendlyDescription)
      }
    }
  }
  
  public func logInUser(email: String, password: String, completion: @escaping (ErrorMessage?) -> Void) {
    
    authService.logIn(email: email, password: password) { error in
      
      if let error = error {
        completion(error.userFriendlyDescription)
      } else {
        completion(nil)
      }
    }
  }
  
  public func updatePassword(to password: String, completion: @escaping (ErrorMessage?) -> Void) {
    
    authService.updatePassword(to: password) { error in
      
      if let error = error {
        completion(error.userFriendlyDescription)
      } else {
        completion(nil)
      }
    }
  }
  
  public func logOut() {
    
    authService.logOut()
  }
}
