//
//  ViewModel.h
//  ViewModel
//
//  Created by Andrei Budisan on 22/04/2020.
//  Copyright © 2020 Andrei Budisan. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for ViewModel.
FOUNDATION_EXPORT double ViewModelVersionNumber;

//! Project version string for ViewModel.
FOUNDATION_EXPORT const unsigned char ViewModelVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <ViewModel/PublicHeader.h>


