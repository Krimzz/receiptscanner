//
//  AuthenticationDelegate.swift
//  Networking
//
//  Created by Andrei Budisan on 22/04/2020.
//  Copyright © 2020 Andrei Budisan. All rights reserved.
//

import Foundation

public protocol AuthenticationDelegate: class {
    
  func authorize(request: URLRequest,
                 _ completion: @escaping(Result<URLRequest, Error>) -> Void)
  
  func requestNewIdToken(completion: @escaping (Bool) -> Void)
}
