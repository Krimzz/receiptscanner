//
//  RSNetworkAdapter.swift
//  Networking
//
//  Created by Andrei Budisan on 22/04/2020.
//  Copyright © 2020 Andrei Budisan. All rights reserved.
//

import Foundation
import Alamofire

class RSNetworkAdapter: RequestAdapter {
  
  public weak var delegate: AuthenticationDelegate?
  
  func adapt(_ urlRequest: URLRequest,
             for session: Session,
             completion: @escaping (Result<URLRequest, Error>) -> Void) {
    
    guard let authorizationDelegate = delegate else {

      let urlString = urlRequest.url?.absoluteString ?? "missing"
      log.warning("Tried to adapt URL: \(urlString) with no AuthorizationDelegate set.")
      completion(.success(urlRequest))
      return
    }

    authorizationDelegate.authorize(request: urlRequest,
                                    completion)
  }
}
