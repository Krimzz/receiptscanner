//
//  RSNetworkRetrier.swift
//  Networking
//
//  Created by Andrei Budisan on 22/04/2020.
//  Copyright © 2020 Andrei Budisan. All rights reserved.
//

import Foundation
import Alamofire

class RSNetworkRetrier: RequestRetrier {
  
  public weak var delegate: AuthenticationDelegate?
  
  private typealias RetryCompletion = (RetryResult) -> Void
  
  private let lock = NSLock()
  private var retrying = false
  private var pendingRetries = [RetryCompletion]()
  
  func retry(_ request: Request,
             for session: Session,
             dueTo error: Error,
             completion: @escaping (RetryResult) -> Void) {
    
    lock.lock() ; defer { lock.unlock() }
    
    guard let authorizationDelegate = delegate else {
      log.warning("NetworkRetrier will not retry request: \(request) because AuthorizationDelegate is missing.")
      completion(.doNotRetry)
      return
    }
    
    guard let response = request.response else {
      log.error("Request response was nil when attempting to retry.")
      completion(.doNotRetry)
      return
    }
    
    if response.statusCode == 401 {
      
      log.debug("Enqueued request for retry: " + request.description)
      pendingRetries.append(completion)
      guard !retrying else { return }
      
      retrying = true
      authorizationDelegate.requestNewIdToken { succeeded in
        
        self.lock.lock() ; defer { self.lock.unlock() }
        
        if succeeded {
          log.debug("Succesfully retried all requests.")
        } else {
          log.error("Could not retry requests because token refresh failed.")
        }
        
        self.pendingRetries.forEach { $0(succeeded ? .retry : .doNotRetry) }
        self.pendingRetries.removeAll()
        self.retrying = false
      }
    } else {
      log.debug("Response status code was not 401. Not retrying.")
      completion(.doNotRetry)
    }
  }
}
