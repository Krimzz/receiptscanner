//
//  AFDataResponse+CustomDebugStringConvertible.swift
//  Networking
//
//  Created by Andrei Budisan on 22/04/2020.
//  Copyright © 2020 Andrei Budisan. All rights reserved.
//

import Foundation
import Alamofire

extension AFDataResponse {
  
  public var debugDescription: String {

    let body: String? = {
      
      guard let data = self.data else { return nil }
      guard let jsonObject = try? JSONSerialization.jsonObject(with: data, options: []),
        let jsonData = try? JSONSerialization.data(withJSONObject: jsonObject, options: [.prettyPrinted]) else {
          return String(data: data, encoding: .utf8) ?? data.debugDescription
      }
      return String(data: jsonData, encoding: .utf8) ?? data.debugDescription
    }()
    
    return """
    statusCode: \(response?.statusCode ?? 0)
    duration: \(metrics?.taskInterval.duration ?? 0)
    body: \(body ?? "nil")
    """
  }
}
