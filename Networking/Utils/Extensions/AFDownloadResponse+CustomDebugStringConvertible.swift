//
//  AFDownloadResponse+CustomDebugStringConvertible.swift
//  Networking
//
//  Created by Andrei Budisan on 22/04/2020.
//  Copyright © 2020 Andrei Budisan. All rights reserved.
//

import Foundation
import Alamofire

extension AFDownloadResponse {
  
  public var debugDescription: String {

    """
    statusCode \(response?.statusCode ?? 0)
    duration \(metrics?.taskInterval.duration ?? 0)
    destination \(fileURL?.absoluteString ?? "unknown")
    """
  }
}
