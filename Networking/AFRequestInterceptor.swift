//
//  AFRequestInterceptor.swift
//  Networking
//
//  Created by Andrei Budisan on 23/04/2020.
//  Copyright © 2020 Andrei Budisan. All rights reserved.
//

import Foundation
import Alamofire

final class AFRequestInterceptor: RequestInterceptor {

  private let retrier: RSNetworkRetrier
  private let adapter: RSNetworkAdapter

  init(retrier: RSNetworkRetrier,
       adapter: RSNetworkAdapter) {

    self.retrier = retrier
    self.adapter = adapter
  }
  
  func retry(_ request: Request,
             for session: Session,
             dueTo error: Error,
             completion: @escaping (RetryResult) -> Void) {
    
    var networkingError: NetworkingError!
    if let error = error as? NetworkingError {
      networkingError = error
    } else {
      networkingError = NetworkingError(error)
    }
    
    retrier.retry(request, for: session, dueTo: networkingError, completion: completion)
  }
  
  func adapt(_ urlRequest: URLRequest,
             for session: Session,
             completion: @escaping (Result<URLRequest, Error>) -> Void) {
    
    adapter.adapt(urlRequest, for: session, completion: completion)
  }
}
