//
//  NetworkService.swift
//  Networking
//
//  Created by Andrei Budisan on 23/04/2020.
//  Copyright © 2020 Andrei Budisan. All rights reserved.
//

import Foundation

public typealias FilePath = URL

public protocol NetworkService {

  var delegate: AuthenticationDelegate? { get set }
  
  func reachability(completion: @escaping (Bool) -> Void)
  
  func send<T>(request: URLRequest,
               completion: @escaping (Result<T, NetworkingError>) -> Void) where T: Decodable
  
  func download(request: URLRequest,
                to destination: FilePath,
                _ completion: @escaping (NetworkingError?) -> Void)
  
  func upload(image: Data,
              with name: String,
              to url: FilePath,
              completion: @escaping (Result<Data?, NetworkingError>) -> Void)
}
