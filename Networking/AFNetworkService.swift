//
//  AFNetworkService.swift
//  Networking
//
//  Created by Andrei Budisan on 23/04/2020.
//  Copyright © 2020 Andrei Budisan. All rights reserved.
//

import Foundation
import Alamofire

public final class AFNetworkService: NetworkService {
  
  private let session: Session
  private let retrier: RSNetworkRetrier
  private let adapter: RSNetworkAdapter
  
  private let decoder = JSONDecoder()
  
  public weak var delegate: AuthenticationDelegate? {
    
    didSet {
      retrier.delegate = delegate
      adapter.delegate = delegate
    }
  }
  
  public init() {
    retrier = RSNetworkRetrier()
    adapter = RSNetworkAdapter()
    
    let interceptor = AFRequestInterceptor(retrier: retrier,
                                           adapter: adapter)
    
    session = Session(interceptor: interceptor)
  }
  
  public func reachability(completion: @escaping (Bool) -> Void) {
    
    NetworkReachabilityManager()?.startListening(onUpdatePerforming: { status in
      completion(status != .notReachable)
    })
  }
  
  public func send<T>(request: URLRequest,
                      completion: @escaping (Result<T, NetworkingError>) -> Void) where T: Decodable {
    
    session.request(request).validate().response { response in
      
      log.info("""
        Did finish \(request.httpMethod ?? "GET") request \
        for URL: \(request.url?.absoluteString ?? "empty")
        Response \(response.debugDescription).
        """)
      
      switch response.result {
      case .success(let data):
        
        guard let data = data else {
          log.warning("No data received - \(T.self)")
          completion(.failure(.missingServerResponse))
          return
        }
        
        if String(data: data, encoding: .utf8) == "null" {
          log.warning("No data received - \(T.self)")
          completion(.failure(.missingServerResponse))
          return
        }
        
        do {
          let entities = try self.decoder.decode(T.self, from: data)
          completion(.success(entities))
        } catch {
          let dataString = String(describing: String(data: data, encoding: .utf8))
          let message = "Received malformed data: \(dataString)\n for \(T.self)"
          log.warning(message)
          completion(.failure(.decodingError(details: "\(error)")))
        }
        
      case .failure(let afError):
        if let data = response.data, let errorBody = try? JSONDecoder().decode(ErrorResponse.self, from: data) {
          
          let authError = AuthenticationError(rawValue: errorBody.error.message) ?? .unknownError
          completion(.failure(.authenticationError(authError)))
        } else {
          completion(.failure(NetworkingError(afError, data: response.data)))
        }
      }
    }
  }
  
  public func download(request: URLRequest,
                       to destination: FilePath,
                       _ completion: @escaping (NetworkingError?) -> Void) {
    
    let fileDestination: DownloadRequest.Destination = { _, _ in
      (destination, [.removePreviousFile, .createIntermediateDirectories])
    }
    let downloadRequest = session.download(request, to: fileDestination)

    log.verbose("""
      Did start download from URL: \(request.url?.absoluteString ?? "") to \(destination.path)
      """)
    
    downloadRequest
      .validate()
      .response { response in

        log.verbose("""
          Did finish download from URL: \(request.url?.absoluteString ?? "")
          Response: \(response.debugDescription)
          """)

        completion(response.error.map { NetworkingError($0) })
      }
  }
  
  public func upload(image: Data,
                     with name: String,
                     to url: FilePath,
                     completion: @escaping (Result<Data?, NetworkingError>) -> Void) {
    
    let uploadRequest = session.upload(multipartFormData: { multipartFormData in
      multipartFormData.append(image,
                               withName: name,
                               fileName: name,
                               mimeType: "images/jpeg")
    }, to: url, method: .post)
    
    log.verbose("""
      Did start upload request to URL: \(url.absoluteString)
      """)
    
    uploadRequest
      .validate()
      .response { response in
        
        log.verbose("""
          Did finish upload to URL: \(url.absoluteString)
          Response: \(response.debugDescription)
          """)
        
        switch response.result {
        case .success(let data):
          completion(.success(data))
        case .failure(let afError):
          completion(.failure(NetworkingError(afError)))
        }
      }
  }
}
