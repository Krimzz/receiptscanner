//
//  AuthenticationError.swift
//  Services
//
//  Created by Andrei Budisan on 26/04/2020.
//  Copyright © 2020 Andrei Budisan. All rights reserved.
//

import Foundation

public enum AuthenticationError: String, Error {
  
  case tokenExpired = "TOKEN_EXPIRED"
  case invalidRefreshToken = "INVALID_REFRESH_TOKEN"
  case missingRefreshToken = "MISSING_REFRESH_TOKEN"
  case emailExists = "EMAIL_EXISTS"
  case emailNotFound = "EMAIL_NOT_FOUND"
  case invalidPassword = "INVALID_PASSWORD"
  case unknownError = "UNKNOWN_ERROR"
}

extension AuthenticationError: LocalizedError {

  public var errorDescription: String? {
    rawValue
  }
}

extension AuthenticationError {
  
  public var userFriendlyDescription: String {
    switch self {
    case .tokenExpired:
      return "Session expired. Please sign in again"
    case .invalidRefreshToken:
      return "Refresh Token has expired"
    case .missingRefreshToken:
      return "Refresh Token not found"
    case .emailExists:
      return "An account with this email already exists"
    case .emailNotFound:
      return "Cannot find account with this email address"
    case .invalidPassword:
      return "Password is invalid"
    case .unknownError:
      return "Unknown error occured"
    }
  }
}
