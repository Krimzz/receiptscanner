//
//  ErrorResponse.swift
//  Networking
//
//  Created by Andrei Budisan on 26/04/2020.
//  Copyright © 2020 Andrei Budisan. All rights reserved.
//

import Foundation

public struct ErrorResponse: Codable {
  
  var error: Metadata
  
  struct Metadata: Codable {
    
    var code: Int
    var message: String
    var errors: [Errors]
    
    struct Errors: Codable {
      var message: String
      var domain: String
      var reason: String
    }
  }
}
