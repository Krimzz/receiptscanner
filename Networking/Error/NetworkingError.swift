//
//  NetworkingError.swift
//  Networking
//
//  Created by Andrei Budisan on 23/04/2020.
//  Copyright © 2020 Andrei Budisan. All rights reserved.
//

import Foundation
import Alamofire

public enum NetworkingError: Error {
  
  case unknown(message: String)
  
  // MARK: - Network errors
  case noInternetConnection
  case connectionTimeout
  case cannotConnectToServer(reason: String)
  
  // MARK: - Server errors
  case serverError(code: Int, data: Data? = nil)
  case missingServerResponse
  case decodingError(details: String)
  case authenticationError(_ error: AuthenticationError)
}

// MARK: - LocalizedError

extension NetworkingError: LocalizedError {

  public var errorDescription: String? {

    switch self {
      // MARK: - Network errors
      case .noInternetConnection:
        return "Cannot connect to the internet."
      case .connectionTimeout:
        return "Server connection timed out."
      case .cannotConnectToServer(let reason):
        return "Unable to establish server connection. Reason: \(reason)."

      // MARK: - Server errors
      case .serverError(let code, _):
        return "Server error with status code: \(code)."
      case .missingServerResponse:
        return "Server response is missing."
      case .decodingError(let details):
        return "Decoding failed with error \(details)."
      case .authenticationError(let message):
        return "Authentication failed with error: \(message.errorDescription ?? "")"

      case .unknown(let message):
        return "Networking encountered an unknown error. Details: \(message)."
    }
  }
}

extension NetworkingError {

  init(_ error: Error, data: Data? = nil) {

    // MARK: - URLError
    if let urlError = error as? URLError {

      let errorMessage = urlError.localizedDescription

      switch urlError.code {
        case .notConnectedToInternet:
          self = .noInternetConnection
        case .timedOut:
          self = .connectionTimeout
        case .cannotFindHost, .cannotConnectToHost, .dnsLookupFailed, .networkConnectionLost, .badServerResponse:
          self = .cannotConnectToServer(reason: errorMessage)
        default:
          self = .unknown(message: urlError.localizedDescription)
      }
      return
    }

    // MARK: - AFError
    if let afError = error as? AFError {

      switch afError {
        case .responseValidationFailed(let reason):
          switch reason {
            case .unacceptableStatusCode(let code):
              self = .serverError(code: code, data: data)
            default:
              self = .unknown(message: String(describing: reason))
          }
        default:
          self = .unknown(message: afError.localizedDescription)
      }
      return
    }

    self = .unknown(message: error.localizedDescription)
  }
}


// MARK: - Equatable

extension NetworkingError: Equatable {
  
  public static func == (lhs: NetworkingError, rhs: NetworkingError) -> Bool {
    
    switch (lhs, rhs) {
    case (.noInternetConnection, .noInternetConnection),
         (.connectionTimeout, .connectionTimeout),
         (.missingServerResponse, .missingServerResponse):
      return true
    case (.unknown(let lhsErrorMessage), .unknown(let rhsErrorMessage)):
      return lhsErrorMessage == rhsErrorMessage
    case (.cannotConnectToServer(let lhsReason), .cannotConnectToServer(let rhsReason)):
      return lhsReason == rhsReason
    case (.serverError(let lhsErrorCode, _), .serverError(let rhsErrorCode, _)):
      return lhsErrorCode == rhsErrorCode
    case (.decodingError(let lhsDetails), .decodingError(let rhsDetails)):
      return lhsDetails == rhsDetails
    case (.authenticationError(let lhsError), .authenticationError(let rhsError)):
      return lhsError == rhsError
    default:
      return false
    }
  }
}
