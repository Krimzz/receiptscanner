//
//  Log.swift
//  Log
//
//  Created by Andrei Budisan on 07/03/2020.
//  Copyright © 2020 Andrei Budisan. All rights reserved.
//

import Foundation
import SwiftyBeaver

public var log: SwiftyBeaver.Type = {
  
  var beaverLog = SwiftyBeaver.self
  let console = ConsoleDestination()
  
  console.levelColor.error = "❌ "
  console.levelColor.warning = "⚠️ "
  console.levelColor.info = "🔵 "
  console.levelColor.debug = "⚪️ "
  console.levelColor.verbose = " "
    
  beaverLog.addDestination(console)
  
  return beaverLog
}()
